    <?php 
    #Template Name: Send Error Page
    get_header();
    ?>


    <div class="gracias-container">
        <div class="wpb_row vc_row-fluid gracias-holder">
            <div class="pf-fullwidth clearfix">
                <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                    <div class="wpb_wrapper">
                        <div class="wpb_row vc_row-fluid gracias-form-holder">
                            <div class="pf-container">
                                <div class="pf-row">
                                    <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                                        <div class="wpb_wrapper">

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p class="gracias text-center"><strong>No ha sido posible completar tu registro,</strong> por favor inténtalo más tarde.</p>
                                                    <div class="text-center"><a href="" class="regresar-gracias-btn">Regresa al inicio</a></div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    get_footer(); 
    ?>
