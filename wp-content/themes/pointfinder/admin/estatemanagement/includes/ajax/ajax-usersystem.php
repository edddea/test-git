<?php
/**********************************************************************************************************************************
*
* Ajax Member System
* 
* Author: Webbu Design
*
***********************************************************************************************************************************/


add_action( 'PF_AJAX_HANDLER_pfget_usersystem', 'pf_ajax_usersystem' );
add_action( 'PF_AJAX_HANDLER_nopriv_pfget_usersystem', 'pf_ajax_usersystem' );

function pf_ajax_usersystem(){
  
	//Security
  check_ajax_referer( 'pfget_usersystem', 'security');
  
	header('Content-Type: text/html; charset=UTF-8;');

	//Get form type
  if(isset($_POST['formtype']) && $_POST['formtype']!=''){
    $formtype = esc_attr($_POST['formtype']);
  }

  $pfrecheck = PFRECIssetControl('setupreCaptcha_general_status','','0');
  

  $recaptcha1 = '';
  if ($pfrecheck == 1) {
    $recaptcha2 = '<section><div id="recaptcha_div_us">'.PFreCaptchaWidget().'</div></section>';
  }else{
    $recaptcha2 = '';
  }
  

	switch($formtype){
/**
*Login
**/
		case 'login':
    $pfkeyarray = array(
      0 => esc_html__('Account Login','pointfindert2d'), 
      1 => esc_html__('LOGIN WITH TWITTER','pointfindert2d'),
      2 => esc_html__('LOGIN WITH FACEBOOK','pointfindert2d'), 
      3 => esc_html__('Not a member yet?','pointfindert2d'), 
      4 => esc_html__('Register Now','pointfindert2d'), 
      5 => esc_html__('- Its  Free','pointfindert2d'), 
      6 => esc_html__('Username:','pointfindert2d'), 
      7 => esc_html__('Enter Username','pointfindert2d'), 
      8 => esc_html__('Password:','pointfindert2d'), 
      9 => esc_html__('Enter Password','pointfindert2d'), 
      10 => esc_html__('Forgot Password?','pointfindert2d'), 
      11 => esc_html__('Login Now','pointfindert2d'), 
      12 => esc_html__('OR','pointfindert2d'),
      13 => esc_html__('CLOSE','pointfindert2d'),
      14 => esc_html__('Please write username','pointfindert2d'),
      15 => esc_html__('Please write password','pointfindert2d'),
      16 => esc_html__('Remember me','pointfindert2d'),
      17 => esc_html__('YES','pointfindert2d'),
      18 => esc_html__('NO','pointfindert2d'),
      19 => esc_html__('Please enter at least 3 characters for Username.','pointfindert2d'),
    );
	
	$setup4_membersettings_dashboard_link = esc_url(home_url());
	$pfmenu_perout = PFPermalinkCheck();
  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_login_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

    $facebook_login_check = PFSAIssetControl('setup4_membersettings_facebooklogin','','0');
    $twitter_login_check = PFSAIssetControl('setup4_membersettings_twitterlogin','','0');
	if($twitter_login_check == 1){
		$twitter_login_text = <<<EOT
		<section>
		<div class="social-btns full">
		    <a id="pf-ajax-logintwitter" href="{$setup4_membersettings_dashboard_link}{$pfmenu_perout}uaf=twlogin" class="tws"><i class="pfadmicon-glyph-769"></i><span>{$pfkeyarray[1]}</span></a>
		</div>                    
		</section>
EOT;
	}else{$twitter_login_text = '';}

    if($facebook_login_check == 1){
    $facebook_login_text = <<<EOT
    <section>
        <div class="social-btns full">
            <a id="pf-ajax-loginfacebook" href="{$setup4_membersettings_dashboard_link}{$pfmenu_perout}uaf=fblogin" class="fbs"><i class="pfadmicon-glyph-770"></i><span>{$pfkeyarray[2]}</span></a>                            
        </div>                       
    </section>
EOT;
    }else{$facebook_login_text = '';}

    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      $.pfAjaxUserSystemVars = {};
      $.pfAjaxUserSystemVars.username_err = '{$pfkeyarray[14]}';
      $.pfAjaxUserSystemVars.username_err2 = '{$pfkeyarray[19]}';
      $.pfAjaxUserSystemVars.password_err = '{$pfkeyarray[15]}';

      {$recaptcha1}
      })(jQuery);
           
         
      </script>
	     <div class="golden-forms wrapper mini">
            <div id="pflgcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-login-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[13]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                      {$facebook_login_text}
                      {$twitter_login_text}
                       <section>
                       		 <label class="cxb">{$pfkeyarray[3]} <strong><a id="pf-register-trigger-button-inner" class="glink ext">{$pfkeyarray[4]}</a></strong> {$pfkeyarray[5]}</label>
                             <div class="tagline"><span>{$pfkeyarray[12]}</span></div>
                       </section>
                       <section>
                       		<label for="usernames" class="lbl-text">{$pfkeyarray[6]}</label>
                            <label class="lbl-ui append-icon">
                            	<input type="text" name="username" class="input" placeholder="{$pfkeyarray[7]}" autofocus />
                                <span><i class="pfadmicon-glyph-632"></i></span>
                            </label>                           
                       </section> 
                       <section>
                       		<label for="pass" class="lbl-text">{$pfkeyarray[8]}</label>
                            <label class="lbl-ui append-icon">
                                <input type="password" name="password" class="input" placeholder="{$pfkeyarray[9]}" />
                                <span><i class="pfadmicon-glyph-465"></i></span>
                            </label>                            
                       </section>
                       {$recaptcha2}
                       <section>
                          
                          <span class="gtoggle">
                                <label class="toggle-switch blue">
                                  <input type="checkbox" name="rem" id="toggle1_rememberme" />
                                  <label for="toggle1_rememberme" data-on="{$pfkeyarray[17]}" data-off="{$pfkeyarray[18]}"></label>
                                </label>                                
                                <label for="toggle1">{$pfkeyarray[16]} <strong><a id="pf-lp-trigger-button-inner" class="glink ext">{$pfkeyarray[10]}</a></strong></label>
                           </span>  
                       </section> 

                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <button id="pf-ajax-login-button" class="button blue">{$pfkeyarray[11]}</button>
                    </section>                
                </div>
            </form>
        </div>			
EOT;
		break;

/**
*Register
**/
    case 'register':
    $pfkeyarray = array(
      0 => esc_html__('Register an Account','pointfindert2d'), 
      3 => esc_html__('Already have an account?','pointfindert2d'), 
      4 => esc_html__('Login now','pointfindert2d'), 
      6 => esc_html__('Username:','pointfindert2d'), 
      7 => esc_html__('Enter Username','pointfindert2d'), 
      8 => esc_html__('Email:','pointfindert2d'), 
      9 => esc_html__('Enter Email Address','pointfindert2d'), 
      11 => esc_html__('Register Now','pointfindert2d'), 
      10 => esc_html__('OR','pointfindert2d'),
      13 => esc_html__('CLOSE','pointfindert2d'),
      14 => esc_html__('Please write username','pointfindert2d'),
      15 => esc_html__('Please write an email','pointfindert2d'),
      16 => esc_html__('Your email address must be in the format of name@domain.com','pointfindert2d'),
      17 => esc_html__('Please enter at least 3 characters for Username.','pointfindert2d'),
    );
    $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_reg_status','','0');
    if ( $pfrecheck == 1 && $pfrechecklg != 1) {
      $recaptcha1 = '';
      $recaptcha2 = '';
    }
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      $.pfAjaxUserSystemVars2 = {};
      $.pfAjaxUserSystemVars2.username_err = '{$pfkeyarray[14]}';
      $.pfAjaxUserSystemVars2.username_err2 = '{$pfkeyarray[17]}';
      $.pfAjaxUserSystemVars2.email_err = '{$pfkeyarray[15]}';
      $.pfAjaxUserSystemVars2.email_err2 = '{$pfkeyarray[16]}';
      {$recaptcha1}
      })(jQuery);
      </script>
       <div class="golden-forms wrapper mini">
            <div id="pflgcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-register-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[13]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>

                <div class="form-enclose">
                    <div class="form-section">
                       
                       <section>
                           <label class="cxb">{$pfkeyarray[3]} <strong><a id="pf-login-trigger-button-inner" class="glink ext">{$pfkeyarray[4]}</a></strong></label>
                             <div class="tagline"><span>{$pfkeyarray[10]}</span></div>
                       </section>
                       <section>
                          <label for="usernames" class="lbl-text">{$pfkeyarray[6]}</label>
                            <label class="lbl-ui append-icon">
                              <input type="text" name="username" class="input" placeholder="{$pfkeyarray[7]}" autofocus />
                                <span><i class="pfadmicon-glyph-632"></i></span>
                            </label>                           
                       </section> 
                       <section>
                          <label for="pass" class="lbl-text">{$pfkeyarray[8]}</label>
                            <label class="lbl-ui append-icon">
                                <input type="text" name="email" class="input" placeholder="{$pfkeyarray[9]}" />
                                <span><i class="pfadmicon-glyph-823"></i></span>
                            </label>                            
                       </section>
                       {$recaptcha2}
                   </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <button class="button blue" id="pf-ajax-register-button">{$pfkeyarray[11]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;
/**
*Lost Password
**/
    case 'lp':
    $pfkeyarray = array(
      0 => esc_html__('Forgot Password','pointfindert2d'), 
      3 => esc_html__('Please Enter;','pointfindert2d'), 
      4 => esc_html__('Login now','pointfindert2d'), 
      6 => esc_html__('Username:','pointfindert2d'), 
      7 => esc_html__('Enter Username','pointfindert2d'), 
      8 => esc_html__('Email:','pointfindert2d'), 
      9 => esc_html__('Enter Email Address','pointfindert2d'), 
      11 => esc_html__('Send Password','pointfindert2d'), 
      10 => esc_html__('OR','pointfindert2d'),
      12 => esc_html__('Username or Email must be filled.','pointfindert2d'),
      13 => esc_html__('Your email address must be in the format of name@domain.com','pointfindert2d'),
      14 => esc_html__('Please enter at least 3 characters for Username.','pointfindert2d'),
      15 => esc_html__('CLOSE','pointfindert2d'),
    );
    $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_fb_status','','0');
    if ( $pfrecheck == 1 && $pfrechecklg != 1) {
      $recaptcha1 = '';
      $recaptcha2 = '';
    }
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      $.pfAjaxUserSystemVars3 = {};
      $.pfAjaxUserSystemVars3.username_err = '{$pfkeyarray[12]}';
      $.pfAjaxUserSystemVars3.username_err2 = '{$pfkeyarray[14]}';
      $.pfAjaxUserSystemVars3.email_err2 = '{$pfkeyarray[13]}';
      {$recaptcha1}
      })(jQuery);
      </script>
       <div class="golden-forms wrapper mini">
            <div id="pflgcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
            <form id="pf-ajax-lp-form">
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[15]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>

                <div class="form-enclose">
                    <div class="form-section">
                       <section>
                           <label class="lbl-text"><strong>{$pfkeyarray[3]}</strong></label>
                       </section>
                       <section>
                          <label for="usernames" class="lbl-text">{$pfkeyarray[6]}</label>
                            <label class="lbl-ui append-icon">
                              <input type="text" name="username" class="input" placeholder="{$pfkeyarray[7]}" autofocus />
                                <span><i class="pfadmicon-glyph-632"></i></span>
                            </label>                           
                       </section> 
                       <section>
                             <div class="tagline"><span>{$pfkeyarray[10]}</span></div>
                       </section>
                       <section>
                          <label for="pass" class="lbl-text">{$pfkeyarray[8]}</label>
                            <label class="lbl-ui append-icon">
                                <input type="text" name="email" class="input" placeholder="{$pfkeyarray[9]}" />
                                <span><i class="pfadmicon-glyph-823"></i></span>
                            </label>                            
                       </section>
                       {$recaptcha2}
                   </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <button class="button blue" id="pf-ajax-lp-button">{$pfkeyarray[11]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;

/**
*Error Window
**/
    case 'error':
    

	if(isset($_POST['errortype']) && $_POST['errortype']!=''){
		$errortype = esc_attr($_POST['errortype']);
	}
	if (empty($errortype)) {
		$errortype = 0;
	}

	if ($errortype == 1) {
		$pfkeyarray = array(
	      0 => esc_html__('Information','pointfindert2d'), 
	      1 => esc_html__('Details;','pointfindert2d'), 
	      2 => esc_html__('Close','pointfindert2d'), 
	    );
	}elseif($errortype == 0){
		$pfkeyarray = array(
	      0 => esc_html__('Error','pointfindert2d'), 
	      1 => esc_html__('Error Details;','pointfindert2d'), 
	      2 => esc_html__('Close','pointfindert2d'), 
	    );
	}

    echo <<<EOT
       <div class="golden-forms wrapper mini">
            <form id="pf-ajax-cl-form">
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>

                <div class="form-enclose">
                    <div class="form-section">
                       <section>
                           <label class="lbl-text"><strong>{$pfkeyarray[1]}</strong></label>
                           <p id="pf-ajax-cl-details"></p>
                       </section>
                   </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <button class="button blue" id="pf-ajax-cl-button">{$pfkeyarray[2]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;
	}
die();
}

?>