<?php
/**********************************************************************************************************************************
*
* Ajax Modal System
* 
* Author: Webbu Design
*
***********************************************************************************************************************************/


add_action( 'PF_AJAX_HANDLER_pfget_modalsystem', 'pf_ajax_modalsystem' );
add_action( 'PF_AJAX_HANDLER_nopriv_pfget_modalsystem', 'pf_ajax_modalsystem' );

function pf_ajax_modalsystem(){
  
	//Security
  check_ajax_referer( 'pfget_modalsystem', 'security');
  
	header('Content-Type: text/html; charset=UTF-8;');

	//Get form type
  if(isset($_POST['formtype']) && $_POST['formtype']!=''){
    $formtype = esc_attr($_POST['formtype']);
  }

  //Get item id
  if(isset($_POST['itemid']) && $_POST['itemid']!=''){
    $itemid = esc_attr($_POST['itemid']);
    $itemname = ($itemid != '') ? get_the_title($itemid) : '' ;
  }else{
    $itemid = $itemname = '';
  }

   //Get item id
  if(isset($_POST['userid']) && $_POST['userid']!=''){
    $userid = esc_attr($_POST['userid']);
  }else{
    $userid = '';
  }

  $pfrecheck = PFRECIssetControl('setupreCaptcha_general_status','','0');
  

  $recaptcha1 = '';
 
  if ($pfrecheck == 1) {
     $recaptcha2 = '<section><div id="recaptcha_div_mod">'.PFreCaptchaWidget().'</div></section>';
  }else{
    $recaptcha2 = '';
  }

	switch($formtype){
/**
*Enquiry Form
**/
		case 'enquiryform':
    $pfkeyarray = array(
      0 => esc_html__('Contact Form','pointfindert2d'), 
      1 => esc_html__('Name  & Surname','pointfindert2d'), 
      2 => esc_html__('Email Address','pointfindert2d'),
      3 => esc_html__('Phone (Optional)','pointfindert2d'),
      4 => esc_html__('Message','pointfindert2d'),
      5 => esc_html__('Send Contact Form','pointfindert2d'), 
      6 => esc_html__('CLOSE','pointfindert2d'),
      10 => esc_html__('Form Info','pointfindert2d'),
    );
	


  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_con_agent_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

  $val1 = $val2 = $val3 = '';

  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $val2 = $current_user->user_email;

    $val1 = get_user_meta($user_id, 'first_name', true);
    $val1 .= ' '.get_user_meta($user_id, 'last_name', true);
    
    $val3 = get_user_meta($user_id, 'user_mobile', true);
    if ($val3 == '') {
      $val3 = get_user_meta($user_id, 'user_phone', true);
    }
  $namefield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="name" class="input" placeholder="" value="'.$val1.'" />
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="email" class="input" placeholder="" value="'.$val2.'"/>
        </label>                            
    </section>  
    ';
  }else{

    $namefield = '
    <section>
        <label for="names" class="lbl-text">'.$pfkeyarray[1].':</label>
        <label class="lbl-ui">
          <input type="text" name="name" class="input" placeholder=""/>
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label for="email" class="lbl-text">'.$pfkeyarray[2].':</label>
        <label class="lbl-ui">
          <input type="email" name="email" class="input" placeholder=""/>
        </label>                            
    </section>  
    ';

  }

    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      {$recaptcha1}
      })(jQuery);
           
         
      </script>
	     <div class="golden-forms wrapper mini">
            <div id="pfmdcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-enquiry-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[6]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                    <section>
                        <label for="name" class="lbl-text">{$pfkeyarray[10]}:</label>
                        <label class="lbl-ui">
                          {$itemname}
                        </label>                            
                    </section>
                    {$namefield}
                    {$emailfield}
                    
                    <section>
                        <label for="phone" class="lbl-text">{$pfkeyarray[3]}:</label>
                        <label class="lbl-ui">
                          <input type="tel" name="phone" class="input" placeholder="" value="{$val3}"/>
                        </label>                            
                    </section>
                             
                    <section>
                        <label for="msg" class="lbl-text">{$pfkeyarray[4]}:</label>
                        <label class="lbl-ui">
                          <textarea name="msg" class="textarea no-resize" ></textarea>
                        </label>                          
                    </section> 
                    {$recaptcha2}
                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <input type="hidden" name="itemid" class="input" value="{$itemid}"/>
                        <button id="pf-ajax-enquiry-button" class="button blue">{$pfkeyarray[5]}</button>
                    </section>                
                </div>
            </form>
        </div>			
EOT;
		break;

/**
*Enquiry Form Author
**/
    case 'enquiryformauthor':
    $pfkeyarray = array(
      0 => esc_html__('Contact Form','pointfindert2d'), 
      1 => esc_html__('Name  & Surname','pointfindert2d'), 
      2 => esc_html__('Email Address','pointfindert2d'),
      3 => esc_html__('Phone (Optional)','pointfindert2d'),
      4 => esc_html__('Message','pointfindert2d'),
      5 => esc_html__('Send Contact Form','pointfindert2d'), 
      6 => esc_html__('CLOSE','pointfindert2d'),
    
    );
  

  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_con_agent_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

  $val1 = $val2 = $val3 = '';

  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $val2 = $current_user->user_email;

    $val1 = get_user_meta($user_id, 'first_name', true);
    $val1 .= ' '.get_user_meta($user_id, 'last_name', true);
    
    $val3 = get_user_meta($user_id, 'user_mobile', true);
    if ($val3 == '') {
      $val3 = get_user_meta($user_id, 'user_phone', true);
    }

  
    $namefield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="name" class="input" placeholder="" value="'.$val1.'" />
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="email" class="input" placeholder="" value="'.$val2.'"/>
        </label>                            
    </section>  
    ';
  }else{

    $namefield = '
    <section>
        <label for="names" class="lbl-text">'.$pfkeyarray[1].':</label>
        <label class="lbl-ui">
          <input type="text" name="name" class="input" placeholder=""/>
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label for="email" class="lbl-text">'.$pfkeyarray[2].':</label>
        <label class="lbl-ui">
          <input type="email" name="email" class="input" placeholder=""/>
        </label>                            
    </section>  
    ';

  }

    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      {$recaptcha1}
      })(jQuery);
           
         
      </script>
       <div class="golden-forms wrapper mini">
            <div id="pfmdcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-enquiry-form-author">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[6]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                    
                    {$namefield}
                    {$emailfield}  
                    
                    <section>
                        <label for="phone" class="lbl-text">{$pfkeyarray[3]}:</label>
                        <label class="lbl-ui">
                          <input type="tel" name="phone" class="input" placeholder="" value="{$val3}"/>
                        </label>                            
                    </section>
                             
                    <section>
                        <label for="msg" class="lbl-text">{$pfkeyarray[4]}:</label>
                        <label class="lbl-ui">
                          <textarea name="msg" class="textarea no-resize" ></textarea>
                        </label>                          
                    </section> 
                    {$recaptcha2}
                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <input type="hidden" name="userid" class="input" value="{$userid}"/>
                        <button id="pf-ajax-enquiry-button-author" class="button blue">{$pfkeyarray[5]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;

/**
*Report Form
**/
    case 'reportform':
    $pfkeyarray = array(
      0 => esc_html__('Report Form','pointfindert2d'), 
      1 => esc_html__('Name  & Surname','pointfindert2d'), 
      2 => esc_html__('Email Address','pointfindert2d'),
      3 => esc_html__('Phone (Optional)','pointfindert2d'),
      4 => esc_html__('Message','pointfindert2d'),
      5 => esc_html__('Report This!','pointfindert2d'), 
      6 => esc_html__('CLOSE','pointfindert2d'),
      10 => esc_html__('Reported Item','pointfindert2d'),
    );
  


  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_report_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

  $val1 = $val2 = $val3 = $user_id = '';

  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $val2 = $current_user->user_email;

    $val1 = get_user_meta($user_id, 'first_name', true);
    $val1 .= ' '.get_user_meta($user_id, 'last_name', true);
    
    if (empty($val1) || $val1 == ' ') {
      $val1 = $user_id;
    }
    $namefield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="name" class="input" placeholder="" value="'.$val1.'" />
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="email" class="input" placeholder="" value="'.$val2.'"/>
        </label>                            
    </section>  
    ';
  }else{

    $namefield = '
    <section>
        <label for="names" class="lbl-text">'.$pfkeyarray[1].':</label>
        <label class="lbl-ui">
          <input type="text" name="name" class="input" placeholder=""/>
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label for="email" class="lbl-text">'.$pfkeyarray[2].':</label>
        <label class="lbl-ui">
          <input type="email" name="email" class="input" placeholder=""/>
        </label>                            
    </section>  
    ';

  }

    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      {$recaptcha1}
      })(jQuery);
           
         
      </script>

       <div class="golden-forms wrapper mini">
            <div id="pfmdcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-report-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[6]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                    <section>
                        <label for="name" class="lbl-text">{$pfkeyarray[10]}:</label>
                        <label class="lbl-ui">
                          {$itemname}
                        </label>                            
                    </section>
                    {$namefield}
                    {$emailfield}         
                    <section>
                        <label for="msg" class="lbl-text">{$pfkeyarray[4]}:</label>
                        <label class="lbl-ui">
                          <textarea name="msg" class="textarea no-resize" ></textarea>
                        </label>                          
                    </section> 
                    {$recaptcha2}
                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <input type="hidden" name="itemid" class="input" value="{$itemid}"/>
                        <input type="hidden" name="userid" class="input" value="{$user_id}"/>
                        <button id="pf-ajax-report-button" class="button blue">{$pfkeyarray[5]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;



/**
*Claim Form
**/
    case 'claimform':
    $pfkeyarray = array(
      0 => esc_html__('Claim Form','pointfindert2d'), 
      1 => esc_html__('Name  & Surname','pointfindert2d'), 
      2 => esc_html__('Email Address','pointfindert2d'),
      3 => esc_html__('Phone (Optional)','pointfindert2d'),
      4 => esc_html__('Message','pointfindert2d'),
      5 => esc_html__('Claim Now!','pointfindert2d'), 
      6 => esc_html__('CLOSE','pointfindert2d'),
      10 => esc_html__('Claim Item','pointfindert2d'),
      11 => esc_html__('Phone Number','pointfindert2d')
    );
  


  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_report_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

  $val1 = $val2 = $val3 = $user_id = '';

  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $val2 = $current_user->user_email;

    $val1 = get_user_meta($user_id, 'first_name', true);
    $val1 .= ' '.get_user_meta($user_id, 'last_name', true);
    
    if (empty($val1) || $val1 == ' ') {
      $val1 = $user_id;
    }
    $namefield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="name" class="input" placeholder="" value="'.$val1.'" />
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="email" class="input" placeholder="" value="'.$val2.'"/>
        </label>                            
    </section>  
    ';
  }else{

    $namefield = '
    <section>
        <label for="names" class="lbl-text">'.$pfkeyarray[1].':</label>
        <label class="lbl-ui">
          <input type="text" name="name" class="input" placeholder=""/>
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label for="email" class="lbl-text">'.$pfkeyarray[2].':</label>
        <label class="lbl-ui">
          <input type="email" name="email" class="input" placeholder=""/>
        </label>                            
    </section>  
    ';

  }

    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      {$recaptcha1}
      })(jQuery);
           
         
      </script>

       <div class="golden-forms wrapper mini">
            <div id="pfmdcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-claim-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[6]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                    <section>
                        <label for="name" class="lbl-text">{$pfkeyarray[10]}:</label>
                        <label class="lbl-ui">
                          {$itemname}
                        </label>                            
                    </section>
                    {$namefield}
                    {$emailfield} 
                     <section>
                        <label for="phonenum" class="lbl-text">{$pfkeyarray[11]}:</label>
                        <label class="lbl-ui">
                          <input type="phonenum" name="phonenum" class="input" placeholder=""/>
                        </label>                            
                    </section>          
                    <section>
                        <label for="msg" class="lbl-text">{$pfkeyarray[4]}:</label>
                        <label class="lbl-ui">
                          <textarea name="msg" class="textarea no-resize" ></textarea>
                        </label>                          
                    </section> 
                    {$recaptcha2}
                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <input type="hidden" name="itemid" class="input" value="{$itemid}"/>
                        <input type="hidden" name="userid" class="input" value="{$user_id}"/>
                        <button id="pf-ajax-claim-button" class="button blue">{$pfkeyarray[5]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;



/**
*Flag Review Form
**/
    case 'flagreview':
    $pfkeyarray = array(
      0 => esc_html__('Flag Review Form','pointfindert2d'), 
      1 => esc_html__('Name  & Surname','pointfindert2d'), 
      2 => esc_html__('Email Address','pointfindert2d'),
      3 => esc_html__('Phone (Optional)','pointfindert2d'),
      4 => esc_html__('Reason','pointfindert2d'),
      5 => esc_html__('Flag This Review!','pointfindert2d'), 
      6 => esc_html__('CLOSE','pointfindert2d'),
    );
  


  $pfrechecklg = PFRECIssetControl('setupreCaptcha_general_flagrev_status','','0');
  if ( $pfrecheck == 1 && $pfrechecklg != 1) {
    $recaptcha1 = '';
    $recaptcha2 = '';
  }

  $val1 = $val2 = $val3 = $user_id = '';

  if (is_user_logged_in()) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $val2 = $current_user->user_email;

    $val1 = get_user_meta($user_id, 'first_name', true);
    $val1 .= ' '.get_user_meta($user_id, 'last_name', true);
  
    $namefield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="name" class="input" placeholder="" value="'.$val1.'" />
        </label>                            
    </section>
    ';

    $emailfield = '
    <section>
        <label class="lbl-ui">
          <input type="hidden" name="email" class="input" placeholder="" value="'.$val2.'"/>
        </label>                            
    </section>  
    ';
  }
    
    echo <<<EOT
      <script type='text/javascript'>
      (function($) {
      "use strict";
      {$recaptcha1}
      })(jQuery);
           
         
      </script>

       <div class="golden-forms wrapper mini">
            <div id="pfmdcontainer-overlay" class="pftrwcontainer-overlay"></div>
            <form id="pf-ajax-flag-form">
                <div class="pfmodalclose"><i class="pfadmicon-glyph-707"></i></div>
                <div class="pfsearchformerrors">
                  <ul>
                  </ul>
                  <a class="button pfsearch-err-button">{$pfkeyarray[6]}</a>
                </div>
                <div class="form-title">
                    <h2>{$pfkeyarray[0]}</h2>
                </div>
                <div class="form-enclose">
                    <div class="form-section">
                    {$namefield}
                    {$emailfield}         
                    <section>
                        <label for="msg" class="lbl-text">{$pfkeyarray[4]}:</label>
                        <label class="lbl-ui">
                          <textarea name="msg" class="textarea no-resize" ></textarea>
                        </label>                          
                    </section> 
                    {$recaptcha2}
                </div>
                </div>
                <div class="form-buttons">
                    <section>
                        <input type="hidden" name="reviewid" class="input" value="{$itemid}"/>
                        <input type="hidden" name="userid" class="input" value="{$user_id}"/>
                        <button id="pf-ajax-flag-button" class="button blue">{$pfkeyarray[5]}</button>
                    </section>                
                </div>
            </form>
        </div>      
EOT;
    break;


	}
die();
}

?>