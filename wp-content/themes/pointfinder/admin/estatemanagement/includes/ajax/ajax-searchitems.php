<?php
/**********************************************************************************************************************************
*
* Ajax Search Elements GET
* 
* Author: Webbu Design
*
***********************************************************************************************************************************/


add_action( 'PF_AJAX_HANDLER_pfget_searchitems', 'pf_ajax_searchitems' );
add_action( 'PF_AJAX_HANDLER_nopriv_pfget_searchitems', 'pf_ajax_searchitems' );

function pf_ajax_searchitems(){
  
	//Security
	check_ajax_referer( 'pfget_searchitems', 'security');
  
	header('Content-Type: text/html; charset=UTF-8;');


	if(isset($_POST['pfcat']) && $_POST['pfcat']!=''){
		$pfcat = esc_attr($_POST['pfcat']);
	}

    $setup1s_slides = PFSAIssetControl('setup1s_slides','','');
    $formvals = '';
    if(is_array($setup1s_slides)){
        if(isset($_POST['formvals']) && $_POST['formvals']!=''){
			$formvals = esc_attr($_POST['formvals']);
		}

		if(isset($_POST['widget']) && $_POST['widget']!=''){
			$widget = esc_attr($_POST['widget']);
		}
       	
        $PFListSF = new PF_SFSUB_Val();
        foreach ($setup1s_slides as &$value) {
        
            $PFListSF->GetValue($value['title'],$value['url'],$value['select'],$widget,$formvals,$pfcat);
            
        }

        echo $PFListSF->FieldOutput;
        if (!empty($PFListSF->FieldOutput)) {
            echo '<script type="text/javascript">
            (function($) {
                "use strict";
                $(function(){
                '.$PFListSF->ScriptOutput;
                echo '
                });'.$PFListSF->ScriptOutputDocReady;

            echo'   
                
            })(jQuery);
            </script>';
        }
        
        
        unset($PFListSF);
    }
die();
}

?>