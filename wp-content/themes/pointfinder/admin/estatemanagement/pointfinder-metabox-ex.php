<?php
/**********************************************************************************************************************************
*
* Point Finder Item Add Page Metabox.
* 
* Author: Webbu Design
*
***********************************************************************************************************************************/

/**
*Start:Enqueue Styles
**/
function pointfinder_orders_styles_ex(){
	$screen = get_current_screen();
	$setup3_pointposttype_pt1 = PFSAIssetControl('setup3_pointposttype_pt1','','pfitemfinder');
	if ($screen->post_type == $setup3_pointposttype_pt1) {
		wp_register_script(
			'metabox-custom-cf-scriptspf', 
			get_template_directory_uri() . '/admin/core/js/metabox-scripts.js', 
			array('jquery'),
			'1.0.0',
			true
		); 
        wp_enqueue_script('metabox-custom-cf-scriptspf'); 

        wp_register_style('pfsearch-goldenforms-css', get_template_directory_uri() . '/css/golden-forms.css', array(), '1.0', 'all');
		wp_enqueue_style('pfsearch-goldenforms-css');
	}
}
add_action('admin_enqueue_scripts','pointfinder_orders_styles_ex' );
/**
*End:Enqueue Styles
**/



/**
*Start : Add Metaboxes
**/
	function pointfinder_orders_add_meta_box_ex($post_type) {
		$setup3_pointposttype_pt1 = PFSAIssetControl('setup3_pointposttype_pt1','','pfitemfinder');

		if ($post_type == $setup3_pointposttype_pt1) {
			$setup3_pointposttype_pt7s = PFSAIssetControl('setup3_pointposttype_pt7s','','Listing Type');
			$setup3_pointposttype_pt6 = PFSAIssetControl('setup3_pointposttype_pt6','','Features');

			remove_meta_box( 'pointfinderltypesdiv', $setup3_pointposttype_pt1, 'side' );
			remove_meta_box( 'pointfinderfeaturesdiv', $setup3_pointposttype_pt1, 'side' );
			
			add_meta_box(
				'pointfinder_itemdetailcf_process_lt',
				$setup3_pointposttype_pt7s,
				'pointfinder_itemdetailcf_process_lt_function',
				$setup3_pointposttype_pt1,
				'normal',
				'core'
			);

			add_meta_box(
				'pointfinder_itemdetailcf_process',
				esc_html__( 'Additional Details', 'pointfindert2d' ),
				'pointfinder_itemdetailcf_process_function',
				$setup3_pointposttype_pt1,
				'normal',
				'core'
			);
			$setup3_pointposttype_pt6_check = PFSAIssetControl('setup3_pointposttype_pt6_check','','1');
			if ($setup3_pointposttype_pt6_check ) {
				add_meta_box(
					'pointfinder_itemdetailcf_process_fe',
					$setup3_pointposttype_pt6,
					'pointfinder_itemdetailcf_process_fe_function',
					$setup3_pointposttype_pt1,
					'normal',
					'core'
				);
			}

		}

		
	}
	add_action( 'add_meta_boxes', 'pointfinder_orders_add_meta_box_ex', 10,1);
/**
*End : Add Metaboxes
**/



/**
*Start : Listing Type
**/
function pointfinder_itemdetailcf_process_lt_function( $post ) {
	
	$setup4_submitpage_listingtypes_title = PFSAIssetControl('setup4_submitpage_listingtypes_title','','Listing Type');
    $setup4_submitpage_listingtypes_group = PFSAIssetControl('setup4_submitpage_listingtypes_group','','0');
    $setup4_submitpage_listingtypes_group_ex = PFSAIssetControl('setup4_submitpage_listingtypes_group_ex','','1');
    $setup4_submitpage_listingtypes_verror = PFSAIssetControl('setup4_submitpage_listingtypes_verror','','Please select a listing type.');
    $setup4_submitpage_listingtypes_gridview = PFSAIssetControl('setup4_submitpage_listingtypes_gridview','','0');

    $item_defaultvalue = (isset($post)) ? wp_get_post_terms($post->ID, 'pointfinderltypes', array("fields" => "ids")) : '' ;

    echo '<div class="form-field">';
    echo '<section>';  
    $fields_output_arr = array(
        'listname' => 'pfupload_listingtypes',
        'listtype' => 'listingtypes',
        'listtitle' => '',
        'listsubtype' => 'pointfinderltypes',
        'listgroup' => $setup4_submitpage_listingtypes_group,
        'listgroup_ex' => $setup4_submitpage_listingtypes_group_ex,
        'listdefault' => $item_defaultvalue,
        'listmultiple' => 0
    );
    echo PFGetList_forAdmin($fields_output_arr);
    echo '</section>';


    echo '
    <script>
    jQuery(function(){
        jQuery("#pfupload_listingtypes").select2({
            placeholder: "'.esc_html__("Please select","pointfindert2d").'", 
            formatNoMatches:"'.esc_html__("Nothing found.","pointfindert2d").'",
            allowClear: true, ';
			if($setup4_submitpage_listingtypes_gridview == 1){
				echo 'dropdownCssClass: "pointfinder-upload-page",';
			}
			echo '
            minimumResultsForSearch: 10
        });
    });
    </script>
    </div>
    ';
}
/**
*End : Listing Type
**/



/**
*Start : Custom Fields Content
**/
function pointfinder_itemdetailcf_process_function( $post ) {
	echo "<div class='golden-forms'>";
	echo "<section class='pfsubmit-inner pfsubmit-inner-customfields'></section>";
	echo "</div>";
	echo "<script>";
		echo "
		(function($) {
  		'use strict';
			$.pf_getcustomfields_now = function(itemid){

				$.ajax({
			    	beforeSend:function(){
			    		$('.pfsubmit-inner-customfields').pfLoadingOverlay({action:'show'});
			    	},
					url: '".get_template_directory_uri()."/admin/core/pfajaxhandler.php',
					type: 'POST',
					dataType: 'html',
					data: {
						action: 'pfget_fieldsystem',
						id: itemid,
						place:'backend',
						postid:'".get_the_id()."',
						security: '".wp_create_nonce('pfget_fieldsystem')."'
					},
				})
				.done(function(obj) {
					if (obj.length == 0) {
						$('.pfsubmit-inner-customfields').hide();
					}else{
						$('.pfsubmit-inner-customfields').show();
					}
					$('.pfsubmit-inner-customfields').html(obj);
					$('.pfsubmit-inner-customfields').pfLoadingOverlay({action:'hide'});
				});
			}

			$( '#pfupload_listingtypes' ).change(function(){
				$.pf_getcustomfields_now($('#pfupload_listingtypes').val());
			});

			$(function(){
				$.pf_getcustomfields_now($('#pfupload_listingtypes').val());
			});
		})(jQuery);
		";
	echo "</script>";
}
/**
*End : Custom Fields Content
**/


/**
*Start : Features
**/
function pointfinder_itemdetailcf_process_fe_function( $post ) {
	$setup3_pointposttype_pt6_check = PFSAIssetControl('setup3_pointposttype_pt6_check','','1');
	if ($setup3_pointposttype_pt6_check ) {

		echo "<a class='pfitemdetailcheckall'>";
		echo esc_html__('Check All','pointfindert2d');
		echo "</a>";
		echo " / ";
		echo "<a class='pfitemdetailuncheckall'>";
		echo esc_html__('Uncheck All','pointfindert2d');
		echo "</a>";
		echo "<section class='pfsubmit-inner pfsubmit-inner-features'></section>";
		
		echo "<script>";
			echo "
			(function($) {
	  		'use strict';
				$.pf_getfeatures_now = function(itemid){

					$.ajax({
				    	beforeSend:function(){
				    		$('.pfsubmit-inner-features').pfLoadingOverlay({action:'show'});
				    	},
						url: '".get_template_directory_uri()."/admin/core/pfajaxhandler.php',
						type: 'POST',
						dataType: 'html',
						data: {
							action: 'pfget_featuresystem',
							id: itemid,
							place: 'backend',
							postid:'".get_the_id()."',
							security: '".wp_create_nonce('pfget_featuresystem')."'
						},
					})
					.done(function(obj) {
						if (obj.length == 0) {
							$('.pfsubmit-inner-features').hide();
							$('.pfsubmit-inner-features-title').hide();
						}else{
							$('.pfsubmit-inner-features').show();
							$('.pfsubmit-inner-features-title').show();
						}
						$('.pfsubmit-inner-features').html(obj);
						$('.pfsubmit-inner-features').pfLoadingOverlay({action:'hide'});
					});
				}

				$( '#pfupload_listingtypes' ).change(function(){
					$.pf_getfeatures_now($('#pfupload_listingtypes').val());
				});

				$(function(){
					$.pf_getfeatures_now($('#pfupload_listingtypes').val());
				});
										
			})(jQuery);
			";
		echo "</script>";
	}
}
/**
*End : Features
**/


/**
*Start : Save Metadata and other inputs
**/
function pointfinder_item_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Make sure that it is set.
	if ( ! isset( $_POST['pfupload_listingtypes'] ) ) {
		return;
	}

	$pfupload_listingtypes = sanitize_text_field($_POST['pfupload_listingtypes']);

	if (function_exists('get_current_screen')) {
		$screen = get_current_screen();
	}
	
	$setup3_pointposttype_pt1 = PFSAIssetControl('setup3_pointposttype_pt1','','pfitemfinder');
	if (isset($screen)) {
		if ($screen->post_type == $setup3_pointposttype_pt1) {
			/*Listing Type*/
				if(isset($pfupload_listingtypes)){
					if(PFControlEmptyArr($pfupload_listingtypes)){
						$pftax_terms = $pfupload_listingtypes;
					}else if(!PFControlEmptyArr($pfupload_listingtypes) && isset($pfupload_listingtypes)){
						$pftax_terms = array($pfupload_listingtypes);
					}
					wp_set_post_terms( $post_id, $pftax_terms, 'pointfinderltypes');
				}


			/*Custom fields loop*/
				$pfstart = PFCheckStatusofVar('setup1_slides');
				$setup1_slides = PFSAIssetControl('setup1_slides','','');

				if($pfstart == true){

					foreach ($setup1_slides as &$value) {

			          $customfield_statuscheck = PFCFIssetControl('setupcustomfields_'.$value['url'].'_frontupload','','0');
			          $available_fields = array(1,2,3,4,5,7,8,9,14);
			          
			          if(in_array($value['select'], $available_fields) && $customfield_statuscheck != 0){
			           	
			           	$post_value_url = sanitize_text_field($_POST[''.$value['url'].'']);
						if(isset($post_value_url)){
						
							if(!is_array($post_value_url)){ 
								if (PFcheck_postmeta_exist('webbupointfinder_item_'.$value['url'],$post_id)) { 
									update_post_meta($post_id, 'webbupointfinder_item_'.$value['url'], $post_value_url);	
								}else{
									add_post_meta ($post_id, 'webbupointfinder_item_'.$value['url'], $post_value_url);
								}; 
								
							
							}else{

								if(PFcheck_postmeta_exist('webbupointfinder_item_'.$value['url'],$post_id)){
									delete_post_meta($post_id, 'webbupointfinder_item_'.$value['url']);
								};

								foreach (sanitize_text_field($_POST[''.$value['url'].'']) as $val) {
									add_post_meta ($post_id, 'webbupointfinder_item_'.$value['url'], $val);
								};

							};
						}else{
							if (PFcheck_postmeta_exist('webbupointfinder_item_'.$value['url'],$post_id)) { 
								delete_post_meta($post_id, 'webbupointfinder_item_'.$value['url']);
							}; 
						};

			          };
			          
			        };
				};


			/*Features*/
			$setup3_pointposttype_pt6_check = PFSAIssetControl('setup3_pointposttype_pt6_check','','1');
			if ($setup3_pointposttype_pt6_check ) {
				if (!empty($_POST['pffeature'])) {
					$feature_values = PFCleanArrayAttr('PFCleanFilters',$_POST['pffeature']);
				
					if(isset($feature_values)){				
						if(PFControlEmptyArr($feature_values)){
							$pftax_terms = $feature_values;
						}else if(!PFControlEmptyArr($feature_values) && isset($feature_values)){
							$pftax_terms = array($feature_values);
						}
						wp_set_post_terms( $post_id, $pftax_terms, 'pointfinderfeatures');
					}
				}
			}



		}
	}
}
add_action( 'save_post', 'pointfinder_item_save_meta_box_data' );
/**
*End : Save Metadata and other inputs
**/
?>