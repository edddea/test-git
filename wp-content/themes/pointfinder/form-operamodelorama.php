<?php
    get_header();
    #template name: Opera un Modelorama
    $idPost = get_the_ID();
    ?>

    <div id="pf-loading-dialog" class="pftsrwcontainer-overlay"></div>

    <div class="wpf-container">
        <div id="pfmaincontent" class="wpf-container-inner">
            <div class="wpb_row vc_row-fluid opera-holder">
                <div class="pf-fullwidth clearfix">
                    <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <h3>opera un modelorama</h3>

                                </div>
                            </div>
                            <div class="wpb_row vc_row-fluid opera-form-holder">
                                <div class="pf-container">
                                    <div class="pf-row">
                                        <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                                            <div class="wpb_wrapper">

                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p class="question">¿Te interesa operar un Modelorama?</p>
                                                        <p>Consulta la información mínima requerida y compártenos tus datos; nosotros te contactaremos.</p>

                                                        <div class="form-holder">

                                                            <form class="form-opera" name="operaForm" id="operaForm" action="<?php bloginfo('template_url'); ?>/backends/saveOpera.php" method="POST">
                                                                <div class="campos-left" id="campos-left">
                                                                    <label for="nombre">Nombre</label>
                                                                    <input type="text" name="nombre" id="nombre" class="validate[required,custom[onlyLetterSp]]" TABINDEX="1" />
                                                                    <label for="apellidoPaterno">Apellido paterno</label>
                                                                    <input type="text" name="apellidoPaterno" id="apellidoPaterno" class="validate[required,custom[onlyLetterSp]]" TABINDEX="2" />
                                                                    <label for="apellidoMaterno">Apellido materno</label>
                                                                    <input type="text" name="apellidoMaterno" id="apellidoMaterno" class="validate[required,custom[onlyLetterSp]]" TABINDEX="3" />
                                                                    <label for="telefonoFijo">Teléfono fijo</label>
                                                                    <input type="text" name="telefonoFijo" id="telefonoFijo" class="validate[required]" TABINDEX="4" />
                                                                    <label for="telefonoCelular">Teléfono celular</label>
                                                                    <input type="text" name="telefonoCelular" id="telefonoCelular" class="validate[required]" TABINDEX="5" />
                                                                    <div id="colum-left">

                                                                    </div>
                                                                </div>
                                                                <div class="campos-right">
                                                                  <label for="cp">Código Postal</label>
                                                                  <input type="text" name="cp" id="cp" class="validate[required,custom[integer]]" TABINDEX="6" maxlength="5" size="5" alt="Escribe un codigo postal de 5 digitos, para poder continuar con el registro."/>
                                                                    <label for="edo">Estado</label>
                                                                    <input type="text" name="edo" id="edo" readonly class="validate[required]" />
                                                                    <label for="mun">Municipio</label>
                                                                    <input type="text" name="mun" id="mun" readonly class="validate[required]" />
                                                                    <label for="col">Colonia</label>
                                                                    <div class="selec-input">
                                                                        <select name="col" id="col" TABINDEX="7">
                                                                            <option value="" selected>Selecciona tu Colonia</option>
                                                                        </select>
                                                                    </div>
                                                                    <label for="correo">Correo electrónico</label>
                                                                    <input type="text" name="correo" id="correo" class="validate[required,custom[email]]" TABINDEX="8" />

                                                                    <div id="colum-right">

                                                                    </div>
                                                                </div>
                                                                <div>
                                                                  <br>
                                                                  <br>
                                                                  <p class="extra">Existen otros requisitos que cumplir de los cuales serán informados personalmente.</p>
                                                                  <p><input class="btn-enviar" type="submit" value="Registrate" TABINDEX="11" /></p>
                                                                </div>
                                                                <input type="hidden" name="tipo" id="tipo" value="operar" />
                                                                <input type="hidden" name="postID" id="postID" value="<?php echo $idPost; ?>">
                                                            </form>
                                                            <div>
                                                              <p> Documentación requerida:</p>
                                                              <ul class="documentacion">
                                                                  <li>Credencial del INE vigente</li>
                                                                  <li>Comprobante de domicilio vigente</li>
                                                                  <li>Inversión Inicial</li>
                                                                  <li class="last-li">La documentación requerida puede variar de acuerdo a tu lugar de residencia.</li>
                                                              </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p> Documentación requerida:</p>
                                                        <ul class="documentacion">
                                                            <li>Credencial del INE vigente</li>
                                                            <li>Comprobante de domicilio vigente</li>
                                                            <li>Inversión Inicial</li>
                                                            <li class="last-li">La documentación requerida puede variar de acuerdo a tu lugar de residencia.</li>
                                                        </ul>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Row Backgrounds -->
            <div class="upb_bg_img" data-ultimate-bg="url(http://modelorama.vendeapp.mx/wp-content/uploads/2015/04/bg-form-opera2.jpg)" data-image-id="1879" data-ultimate-bg-style="vcpb-default" data-bg-img-repeat="no-repeat" data-bg-img-size="cover"
                data-bg-img-position="" data-parallx_sense="30" data-bg-override="0" data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="repeat"
                data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false" data-custom-vc-row="" data-vc="4.4.4" data-theme-support=""
                data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""></div>

        </div>
    </div>
    <div id="pf-membersystem-dialog"></div>
    <a title="" class="pf-up-but"><i class="pfadmicon-glyph-859"></i></a>

    <script type="text/javascript">
        var templateUrl = "<?php bloginfo('template_url') ?>";
    </script>
    <script type="text/javascript" src="<?php bloginfo('template_url') ?>/js/controllers/operamodelorama.js"></script>

    <?php if(!isset($_GET["isRegister"])){ ?>
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;

        document.write('<iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_0000;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
    </script>
    <noscript>
        <iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_0000;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    </noscript>

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
    <!-- Facebook Conversion Code for MODELORAMA_REGISTRO -->
    <script>
        (function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6037814714476', {
            'value': '0.00',
            'currency': 'MXN'
        }]);
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6037814714476&amp;cd[value]=0.00&amp;cd[currency]=MXN&amp;noscript=1" /></noscript>
    <?php } ?>
    <footer class="wpf-footer clearfix">


    <?php
        get_footer();
    ?>
