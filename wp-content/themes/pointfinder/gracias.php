    <?php
    #Template Name: Thanks You Page
    get_header();
    //echo $_SESSION["saveOpera"];
    ?>


    <div class="gracias-container">
        <div class="wpb_row vc_row-fluid gracias-holder">
            <div class="pf-fullwidth clearfix">
                <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                    <div class="wpb_wrapper">
                        <div class="wpb_row vc_row-fluid gracias-form-holder">
                            <div class="pf-container">
                                <div class="pf-row">
                                    <div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
                                        <div class="wpb_wrapper">

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <?php if(isset($_SESSION["saveOpera"]) && ($_SESSION["saveOpera"]==1)){ ?>
                                                    <p class="gracias text-center"><strong>GRACIAS POR DEJARNOS TUS DATOS,</strong> MUY PRONTO NOS PONDREMOS EN CONTACTO CONTIGO</p>
                                                    <div class="text-center"><a href="<?php echo get_bloginfo('url'); ?>" class="regresar-gracias-btn">Regresa al inicio</a></div>
                                                    <?php } ?>

                                                  <?php if(isset($_SESSION["saveRenta"]) && ($_SESSION["saveRenta"]==1)){ ?>
                                                    <p class="gracias text-center"><strong>GRACIAS POR DEJARNOS TUS DATOS,</strong> MUY PRONTO NOS PONDREMOS EN CONTACTO CONTIGO</p>
                                                    <div class="text-center"><a href="<?php echo get_bloginfo('url'); ?>" class="regresar-gracias-btn">Regresa al inicio</a></div>
                                                    <?php } ?>

                                                  <?php if(isset($_SESSION["saveVende"]) && ($_SESSION["saveVende"]==1)){ ?>
                                                    <p class="gracias text-center"><strong>GRACIAS POR DEJARNOS TUS DATOS,</strong> MUY PRONTO NOS PONDREMOS EN CONTACTO CONTIGO</p>
                                                    <div class="text-center"><a href="<?php echo get_bloginfo('url'); ?>" class="regresar-gracias-btn">Regresa al inicio</a></div>
                                                    <?php } ?>

                                                    <?php while ( have_posts() ) : the_post(); ?>
                                                    <?php the_content(); ?>
                                                        <?php //comments_template( '', true ); ?>
                                                    <?php endwhile; // end of the loop. ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    get_footer();
    ?>
