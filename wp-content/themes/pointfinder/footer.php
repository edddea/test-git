        </div>
        </div>
        <div id="pf-membersystem-dialog"></div>
        <a title="<?php esc_html__('Back to Top','pointfindert2d'); ?>" class="pf-up-but"><i class="pfadmicon-glyph-859"></i></a>
        <footer class="wpf-footer clearfix">
        <!-- = START footer MEME -->
            <div class="row">
                <div class="col-lg-6 col-sm-4 col-xs-12">
                    <div class="navigation-footer-holder">
                        <ul class="navigation">
                            <li><a href="#">Quiénes Somos</a></li>
                            <li><a href="mailto:contactomodelorama@gmodelo.com.mx">Contacto</a></li>
                            <li><a href="#">Preguntas Frecuentes</a></li>
                            <li><a href="http://modelorama.com.mx/?page_id=2222">Aviso de Privacidad</a></li>
                        </ul>
                    </div>        
                </div>
                <div class="col-lg-6 col-sm-8 col-xs-12">
                    <div class="footer-right">
                        <div class="social-holder">
                            <ul class="social">
                                <li><a href="https://www.amigosmodelo.com/" class="amigos-modelo" target="_blank">Amigos Modelo</a></li>
                                <li><a href="https://www.beerhouse.mx/" class="beerhouse" target="_blank">Beerhouse</a></li>
                                <li class="bloket"></li>
                                <li class="siguenos">Síguenos</li>
                                <li><a href="#" class="yotube">You Tube</a></li>
                                <li><a href="#" class="linkin">Linkedin</a></li>
                            </ul>
                        </div>
                        <p class="g-modelo">Grupo Modelo 2015 ©</p>
                    </div>
                </div>
            </div>
        <!-- = END footer MEME -->

        <?php
        /* = footer original del tema
        ----------------------------------------------------------------------------------------------------------------
        $setup_footerbar_text_copy = PFSAIssetControl('setup_footerbar_text_copy','','');
        $setup_footerbar_width = PFSAIssetControl('setup_footerbar_width','','0');

        if ($setup_footerbar_width == 0) {
          echo '<div class="pf-container"><div class="pf-row clearfix">';
        }
        ?>
        <div class="wpf-footer-text col-lg-12">
          <?php echo wp_kses_post($setup_footerbar_text_copy);?>

        </div>
        <?php 
        if (PFSAIssetControl('setup_footerbar_text_copy_align','','left') == 'right') {
           $footer_menu_text = ' pfleftside';
        }else{
            $footer_menu_text = ' pfrightside';
        }
        echo '<ul class="pf-footer-menu'.$footer_menu_text.'">';pointfinder_footer_navigation_menu();echo '</ul>';?>
        <?php 
        if ($setup_footerbar_width == 0) {
          echo '</div></div>';
        }
        ?>
        </footer>
        <?php
        /*
        $general_viewoptions = PFSAIssetControl('general_viewoptions','','1');
        if ($general_viewoptions == 0) {
            echo '</div>';
        }
        */
        ?>
        
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1656963864540956');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1656963864540956&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P2SMHV"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-P2SMHV');</script>

<!-- End Google Tag Manager -->



		<?php wp_footer(); ?>
	</body>
</html>