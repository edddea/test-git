<?php 
    /** Error reporting */
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);

    require_once ( '/var/www/vhosts/modelorama.com.mx/httpdocs/development/wp-content/themes/pointfinder/backends/Classes/PHPExcel.php' );
    require_once( "/var/www/vhosts/modelorama.com.mx/httpdocs/development/wp-content/themes/pointfinder/backends/Classes/PHPmailer/PHPMailerAutoload.php" );
    require_once( "/var/www/vhosts/modelorama.com.mx/httpdocs/development/wp-config.php" );

    /*--------------------------------------------
    	REPORTE 01
    ---------------------------------------------*/
    // Intancia de PHPExcel
    $oPHPExcelReport01 = new PHPExcel();

    /**
     * CREACIÓN DE LA HOJA NÚMERO 1
     */
    $oPHPExcelReport01->createSheet(0);
    $oPHPExcelReport01->setActiveSheetIndex(0);

    // Título de la hoja
    $oWinnersSheet = $oPHPExcelReport01->getActiveSheet()->setTitle('Usuarios registros web');

    // Query
    $sQuery = "SELECT id_usuario,
    				  nombre,
    				  ap_paterno,
    				  ap_materno,
    				  tel_fijo,
    				  tel_cel,
    				  email,
    				  cp,
    				  pass,
    				  fecha_registro,
    				  tipo_form
            FROM usuarios_registros_web
            WHERE MONTH(fecha_registro) = MONTH(NOW())";

    $aData = $wpdb->get_results( $sQuery );

    $iNumWinners = count( $aData );

    // Cabeceras
     $oWinnersSheet->setCellValue('A1', 'NO.')  
                ->setCellValue('B1', 'ID USUARIO')  
                ->setCellValue('C1', 'NOMBRE')
                ->setCellValue('D1', 'APELLIDO PATERNO')
                ->setCellValue('E1', 'APELLIDO MATERNO')
                ->setCellValue('F1', 'TELEFONO FIJO')
                ->setCellValue('G1', 'TELEFONO CELULAR')
                ->setCellValue('H1', 'EMAIL')
                ->setCellValue('I1', 'CÓDIGO POSTAL')
                ->setCellValue('J1', 'PASSWORD')
                ->setCellValue('K1', 'FECHA DE REGISTRO')
                ->setCellValue('L1', 'TIPO FORMULARIO');

    // Estilo
    $styleArray = array(
                        'font' => array(
                                        'bold'  => true,
                                        'color' => array('rgb' => '000000'),
                                        'size'  => 12
                                    )
                        );

    $oWinnersSheet->getStyle('A1:L1')->applyFromArray( $styleArray );

    // Alto de las filas
    $oPHPExcelReport01->getActiveSheet()->getRowDimension()->setRowHeight(25);
    // Ancho de las celdas
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('H')->setWidth(35);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('J')->setWidth(25);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('K')->setWidth(25);
    $oPHPExcelReport01->getActiveSheet()->getColumnDimension('L')->setWidth(25);

    $Line = 2;

    /**
     * Generación del contenido
     */
    $count = 0;

    foreach ($aData as $key => $value) {

        $count++;

        $oWinnersSheet->setCellValue( 'A'.$Line, $count )
                    ->setCellValue( 'B'.$Line, $aData[ $key ]->id_usuario )
                    ->setCellValue( 'C'.$Line, $aData[ $key ]->nombre )
                    ->setCellValue( 'D'.$Line, $aData[ $key ]->ap_paterno )
                    ->setCellValue( 'E'.$Line, $aData[ $key ]->ap_materno )
                    ->setCellValue( 'F'.$Line, $aData[ $key ]->tel_fijo )
                    ->setCellValue( 'G'.$Line, $aData[ $key ]->tel_cel )
                    ->setCellValue( 'H'.$Line, $aData[ $key ]->email )
                    ->setCellValue( 'I'.$Line, $aData[ $key ]->cp )
                    ->setCellValue( 'J'.$Line, $aData[ $key ]->pass )
                    ->setCellValue( 'K'.$Line, $aData[ $key ]->fecha_registro )
                    ->setCellValue( 'L'.$Line, $aData[ $key ]->tipo_form );

        ++$Line;

    }

    /**
     * Generación del Archivo
     */
    @ob_start();
    $objWriter = PHPExcel_IOFactory::createWriter($oPHPExcelReport01, 'Excel5');
    $objWriter->save('php://output');
    $dataReport01 = @ob_get_contents();
    @ob_end_clean();
    
    /**
    * Envío del email  
    */
   	// Reporte No. 01
	$sEmailReceiver01 = "contactomodelorama@gmodelo.com.mx";
	$sEmailSubject01  = "REPORTE USUARIOS OPERA";
	$sFileName01      = "reporte-opera-";

	$oMailReport01       = new SendMail();
	$oSendStatus = $oMailReport01->enviar( $sEmailReceiver01, $dataReport01, $sEmailSubject01, $sFileName01 );

	/*--------------------------------------------
		REPORTE 02
	---------------------------------------------*/

    // Intancia de PHPExcel
    $oPHPExcelReport02 = new PHPExcel();

    /**
     * CREACIÓN DE LA HOJA NÚMERO 1
     */
    $oPHPExcelReport02->createSheet(0);
    $oPHPExcelReport02->setActiveSheetIndex(0);

    // Título de la hoja
    $oWinnersSheet = $oPHPExcelReport02->getActiveSheet()->setTitle('Usuarios registros web');

    // Query
    $sQuery = "SELECT t1.id_usuario,
			       t1.nombre,
			       t1.ap_paterno,
			       t1.ap_materno,
			       t1.tel_fijo,
			       t1.tel_cel,
			       t1.email,
			       t1.cp,
			       t1.pass,
			       t1.fecha_registro,
			       t1.tipo_form,
			       t1.renta_solicitada,
			       t1.calle_numero,
			       t1.dimensiones,
			       t1.comentarios,
			       t2.asentamiento,
			       t2.municipio,
			       t2.estado,
			       t2.ciudad
			FROM usuarios_registros_web_rentar AS t1
			INNER JOIN mo_codigos_postales AS t2 ON t1.codigo_postal_id = t2.codigos_postales_id
			WHERE MONTH(t1.fecha_registro) = MONTH(NOW())";

    $aData = $wpdb->get_results( $sQuery );

    $iNumWinners = count( $aData );

    // Cabeceras
     $oWinnersSheet->setCellValue('A1', 'NO.')  
                ->setCellValue('B1', 'ID USUARIO')  
                ->setCellValue('C1', 'NOMBRE')
                ->setCellValue('D1', 'APELLIDO PATERNO')
                ->setCellValue('E1', 'APELLIDO MATERNO')
                ->setCellValue('F1', 'TELEFONO FIJO')
                ->setCellValue('G1', 'TELEFONO CELULAR')
                ->setCellValue('H1', 'EMAIL')
                ->setCellValue('I1', 'CÓDIGO POSTAL')
                ->setCellValue('J1', 'PASSWORD')
                ->setCellValue('K1', 'FECHA DE REGISTRO')
                ->setCellValue('L1', 'TIPO FORM')
                ->setCellValue('M1', 'RENTA SOLICITADA')
                ->setCellValue('N1', 'CALLE NÚMERO')
                ->setCellValue('O1', 'DIMENSIONES')
                ->setCellValue('P1', 'COMENTARIOS')
                ->setCellValue('Q1', 'ASENTAMIENTO')
                ->setCellValue('R1', 'MUNICIPIO')
                ->setCellValue('S1', 'ESTADO')
                ->setCellValue('T1', 'CIUDAD');

    // Estilo
    $styleArray = array(
                        'font' => array(
                                        'bold'  => true,
                                        'color' => array('rgb' => '000000'),
                                        'size'  => 12
                                    )
                        );

    $oWinnersSheet->getStyle('A1:T1')->applyFromArray( $styleArray );

    // Alto de las filas
    $oPHPExcelReport02->getActiveSheet()->getRowDimension()->setRowHeight(25);
    // Ancho de las celdas
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('H')->setWidth(35);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('J')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('K')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('L')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('M')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('N')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('O')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('P')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('R')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('S')->setWidth(25);
    $oPHPExcelReport02->getActiveSheet()->getColumnDimension('T')->setWidth(25);

    $Line = 2;

    /**
     * Generación del contenido
     */
    $count = 0;

    foreach ($aData as $key => $value) {

        $count++;

        $oWinnersSheet->setCellValue( 'A'.$Line, $count )
                    ->setCellValue( 'B'.$Line, $aData[ $key ]->id_usuario )
                    ->setCellValue( 'C'.$Line, $aData[ $key ]->nombre )
                    ->setCellValue( 'D'.$Line, $aData[ $key ]->ap_paterno )
                    ->setCellValue( 'E'.$Line, $aData[ $key ]->ap_materno )
                    ->setCellValue( 'F'.$Line, $aData[ $key ]->tel_fijo )
                    ->setCellValue( 'G'.$Line, $aData[ $key ]->tel_cel )
                    ->setCellValue( 'H'.$Line, $aData[ $key ]->email )
                    ->setCellValue( 'I'.$Line, $aData[ $key ]->cp )
                    ->setCellValue( 'J'.$Line, $aData[ $key ]->pass )
                    ->setCellValue( 'K'.$Line, $aData[ $key ]->fecha_registro )
                    ->setCellValue( 'L'.$Line, $aData[ $key ]->tipo_form )
                    ->setCellValue( 'M'.$Line, $aData[ $key ]->renta_solicitada )
                    ->setCellValue( 'N'.$Line, $aData[ $key ]->calle_numero )
                    ->setCellValue( 'O'.$Line, $aData[ $key ]->dimensiones )
                    ->setCellValue( 'P'.$Line, $aData[ $key ]->comentarios )
                    ->setCellValue( 'Q'.$Line, $aData[ $key ]->asentamiento )
                    ->setCellValue( 'R'.$Line, $aData[ $key ]->municipio )
                    ->setCellValue( 'S'.$Line, $aData[ $key ]->estado )
                    ->setCellValue( 'T'.$Line, $aData[ $key ]->ciudad );

        ++$Line;

    }

    /**
     * Generación del Archivo
     */
    @ob_start();
    $objWriter = PHPExcel_IOFactory::createWriter($oPHPExcelReport02, 'Excel5');
    $objWriter->save('php://output');
    $dataReport02 = @ob_get_contents();
    @ob_end_clean();
    
    /**
    * Envío del email  
    */
   	// Reporte No. 02
    $sEmailReceiver02 = "contactomodelorama@gmodelo.com.mx";
    $sEmailSubject02  = "REPORTE USUARIOS RENTAS";
    $sFileName02      = "reporte-renta-";

	$oMailReport02 = new SendMail();
	$oSendStatus   = $oMailReport02->enviar( $sEmailReceiver02, $dataReport02, $sEmailSubject02, $sFileName02 );

	/*--------------------------------------------
		REPORTE 03
	---------------------------------------------*/

    // Intancia de PHPExcel
    $oPHPExcelReport03 = new PHPExcel();

    /**
     * CREACIÓN DE LA HOJA
     */
    $oPHPExcelReport03->createSheet(0);
    $oPHPExcelReport03->setActiveSheetIndex(0);

    // Título de la hoja
    $oWinnersSheet = $oPHPExcelReport03->getActiveSheet()->setTitle('Usuarios registros web');

    // Query
    $sQuery = "SELECT t1.id_registro_for_venta,
			       t1.nombre_empresa,
			       t1.rfc,
			       t1.calle_numero,
			       t1.curricula_url,
			       t1.empresa_tel,
			       t1.nombre_representante,
			       t1.telefono_representante,
			       t1.email_representante,
			       t1.categoria_producto,
			       t1.clientes_y_lugares,
			       t1.precio_venta_final,
			       t1.precio_compra,
			       t1.descripcion_productos,
			       t1.comentarios,
			       t1.fecha_registro,
			       t2.asentamiento,
			       t2.municipio,
			       t2.estado,
			       t2.ciudad
			FROM usuarios_venta_productos AS t1
			INNER JOIN mo_codigos_postales AS t2 ON t1.codigo_postal_id = t2.codigos_postales_id
			WHERE MONTH(t1.fecha_registro) = MONTH(NOW())";

    $aData = $wpdb->get_results( $sQuery );

    $iNumWinners = count( $aData );

    // Cabeceras
     $oWinnersSheet->setCellValue('A1', 'NO.')  
                ->setCellValue('B1', 'ID REGISTRO POR VENTA')  
                ->setCellValue('C1', 'NOMBRE DE LA EMPRESA')
                ->setCellValue('D1', 'RFC')
                ->setCellValue('E1', 'CALLE Y NÚMERO')
                ->setCellValue('F1', 'CURRICULA URL')
                ->setCellValue('G1', 'TELEFONO DE LA EMPRESA')
                ->setCellValue('H1', 'NOMBRE DEL REPRESENTANTE')
                ->setCellValue('I1', 'TELEFONO DEL REPRESENTANTE')
                ->setCellValue('J1', 'EMAIL DEL REPRESENTANTE')
                ->setCellValue('K1', 'CATEGORÍA DEL PRODUCTO')
                ->setCellValue('L1', 'CLIENTES Y LUGARES')
                ->setCellValue('M1', 'PRECIO DE VENTA FINAL')
                ->setCellValue('N1', 'PRECIO DE COMPRA')
                ->setCellValue('O1', 'DESCRIPCIÓN DEL PRODUCTO')
                ->setCellValue('P1', 'COMENTARIOS')
                ->setCellValue('Q1', 'FECHA DE REGISTRO')
                ->setCellValue('R1', 'ASENTAMIENTO')
                ->setCellValue('S1', 'MUNICIPIO')
                ->setCellValue('T1', 'ESTADO')
                ->setCellValue('U1', 'CIUDAD');

    // Estilo
    $styleArray = array(
                        'font' => array(
                                        'bold'  => true,
                                        'color' => array('rgb' => '000000'),
                                        'size'  => 12
                                    )
                        );

    $oWinnersSheet->getStyle('A1:U1')->applyFromArray( $styleArray );

    // Alto de las filas
    $oPHPExcelReport03->getActiveSheet()->getRowDimension()->setRowHeight(25);
    // Ancho de las celdas
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('A')->setWidth(10);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('C')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('G')->setWidth(18);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('H')->setWidth(35);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('I')->setWidth(20);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('J')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('K')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('L')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('M')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('N')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('O')->setWidth(35);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('P')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('R')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('S')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('T')->setWidth(25);
    $oPHPExcelReport03->getActiveSheet()->getColumnDimension('U')->setWidth(25);

    $Line = 2;

    /**
     * Generación del contenido
     */
    $count = 0;

    foreach ($aData as $key => $value) {

        $count++;

        $oWinnersSheet->setCellValue( 'A'.$Line, $count )
                    ->setCellValue( 'B'.$Line, $aData[ $key ]->id_registro_for_venta )
                    ->setCellValue( 'C'.$Line, $aData[ $key ]->nombre_empresa )
                    ->setCellValue( 'D'.$Line, $aData[ $key ]->rfc )
                    ->setCellValue( 'E'.$Line, $aData[ $key ]->calle_numero )
                    ->setCellValue( 'F'.$Line, $aData[ $key ]->curricula_url )
                    ->setCellValue( 'G'.$Line, $aData[ $key ]->empresa_tel )
                    ->setCellValue( 'H'.$Line, $aData[ $key ]->nombre_representante )
                    ->setCellValue( 'I'.$Line, $aData[ $key ]->telefono_representante )
                    ->setCellValue( 'J'.$Line, $aData[ $key ]->email_representante )
                    ->setCellValue( 'K'.$Line, $aData[ $key ]->categoria_producto )
                    ->setCellValue( 'L'.$Line, $aData[ $key ]->clientes_y_lugares )
                    ->setCellValue( 'M'.$Line, $aData[ $key ]->precio_venta_final )
                    ->setCellValue( 'N'.$Line, $aData[ $key ]->precio_compra )
                    ->setCellValue( 'O'.$Line, $aData[ $key ]->descripcion_productos )
                    ->setCellValue( 'P'.$Line, $aData[ $key ]->comentarios )
                    ->setCellValue( 'Q'.$Line, $aData[ $key ]->fecha_registro )
                    ->setCellValue( 'R'.$Line, $aData[ $key ]->asentamiento )
                    ->setCellValue( 'S'.$Line, $aData[ $key ]->municipio )
                    ->setCellValue( 'T'.$Line, $aData[ $key ]->estado )
                    ->setCellValue( 'U'.$Line, $aData[ $key ]->ciudad );

        ++$Line;

    }

    /**
     * Generación del Archivo
     */
    @ob_start();
    $objWriter = PHPExcel_IOFactory::createWriter($oPHPExcelReport03, 'Excel5');
    $objWriter->save('php://output');
    $dataReport03 = @ob_get_contents();
    @ob_end_clean();
    
    /**
    * Envío del email  
    */
   	// Reporte No. 03
    $sEmailReceiver03 = "contactomodelorama@gmodelo.com.mx";
    $sEmailSubject03  = "REPORTE USUARIOS VENTA";
    $sFileName03      = "reporte-venta-";

	$oMailReport03 = new SendMail();
	$oSendStatus   = $oMailReport03->enviar( $sEmailReceiver03, $dataReport03, $sEmailSubject03, $sFileName03 );


    /**
     * Clase para enviar el E-mail
     */
    class SendMail{
        private $_mail;

        public function __construct()
        {
            $this->_mail = new PHPMailer(true);
            $this->_mail->IsSMTP();
            $this->_mail->SMTPAuth   = true;                               
            $this->_mail->SMTPSecure = "tls";                              
            $this->_mail->Host       = "smtp.mandrillapp.com"; // SERVIDOR SMTP de correo
            $this->_mail->Port       =  587; //PUERTO DEL SERVIDOR SMTP de correo
            
            $this->_mail->Username   = "raul@torodigital.com.mx"; // USERNAME del correo electronico que envia 
            $this->_mail->Password   = "7wppm_jKVPRrqUckA4MA7w"; // PASSWORD del correo electronico que envia 
            
            
            $this->_mail->From       = "reportes@modelorama.com.mx"; //Correo del que envia 
            $this->_mail->FromName   = "Reportes Modelorama"; //Nombre del que envia
            
            // $this->_mail->Subject    = "Reporte Opera"; //Asunto del correo
            $this->_mail->CharSet    = 'UTF-8';
        }

        // Comentarios
        function enviar( $sEmailReceiver, $aData, $sEmailSubject, $sFileName )
        {

            $sMessageMailing = file_get_contents('/var/www/vhosts/modelorama.com.mx/httpdocs/development/wp-content/themes/pointfinder/backends/template-mail.html'); 
            $sMessageMailing = str_replace('%subject%', $sEmailSubject, $sMessageMailing); 

            try{
            	$this->_mail->Subject = $sEmailSubject; //Asunto del correo

                $this->_mail->AddAddress( $sEmailReceiver ); // Destinatario 

                $this->_mail->Body = $sMessageMailing; //Mensaje

                /*$this->_mail->AddAttachment( $aData['tmp_name'],
                                         $aData['name']);*/
				$this->_mail->AddStringAttachment( $aData, $sFileName . date('d-m-Y') . '.xls');

                $this->_mail->IsHTML(true); // Enviar en formato HTML
                
                $this->_mail->send();
                
                return true;        
            }catch (phpmailerException $e) {
                echo $e->errorMessage(); //Pretty error messages from PHPMailer
                return false;
            }catch (Exception $e) {
                echo $e->getMessage(); //Boring error messages from anything else!
                return false;
            }
        }

    }
?>