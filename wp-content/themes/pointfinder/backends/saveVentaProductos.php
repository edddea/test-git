<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once( "Classes/PHPmailer/PHPMailerAutoload.php" );
require_once( "../../../../wp-config.php" );
global $wpdb;
/**/
/*Subiumos el archivo de la curricula*/
$fileName = $_FILES["cv"]["name"];
$fileTmpName = $_FILES["cv"]["tmp_name"];
$pathToSave = "../curriculas_registros/";
$pathToSaveDataBase = "curriculas_registros/";
$finalFileName = time()."_".$fileName;
$type = $_FILES["cv"]["type"];

$file_error = $_FILES["cv"]["error"];

if($file_error != 0){
	echo "Error el archivo PDF de la curricula debe de pesar menos de 5M, da click en el boton de atras y vuelve a subir el archivo de la curricula de la empresa en formato PDF";
}else if ($file_error > 0){
	echo "Error al subir el archivo PDF, da click en el boton de atras y vuelve a subir el archivo de la curricula de la empresa en formato PDF";
}
else{

if($type=="application/pdf"){

if(move_uploaded_file($fileTmpName, $pathToSave.$finalFileName)){

	date_default_timezone_set('America/Mexico_City');

	$nombre_empresa = $_POST["nombreEmpresa"];
	$rfc = $_POST["rfc"];
	$calle_numero = $_POST["calle"];
	$codigo_postal = $_POST["cpEmpresa"];
	$edo = $_POST["edo"];
	$mun = $_POST["mun"];
	$col = $_POST["col"];
	$curricula_url = $finalFileName;
	$empresa_tel = $_POST["telefonoEmpresa"];
	$nombre_representante = $_POST["nombre"];
	$telefono_representante = $_POST["telefono"];
	$email_representante = $_POST["correo"];
	$categoria_producto = $_POST["categoriaProducto"];
	$clientes_y_lugares = $_POST["clientesProducto"];
	$precio_venta_final = $_POST["precioFinal"];
	$precio_compra = $_POST["precioCompra"];
	$descripcion_productos = $_POST["descripcionProducto"];
	$comentarios = $_POST["comentarios"];
	$fecha_registro =  date("Y-m-d H:i:s");

	$postID = $_POST["postID"];

	$aDataToSend = array("nombre_empresa"=>$nombre_empresa,
						 "rfc"=>$rfc,
						 "calle_numero"=>$calle_numero,
						 "codigo_postal"=>$codigo_postal,
						 "estado"=>$edo,
						 "municipio"=>$mun,
						 "colonia"=>$col,
						 "curricula_url"=>$curricula_url,
						 "empresa_tel"=>$empresa_tel,
						 "nombre_representante"=>$nombre_representante,
						 "telefono_representante"=>$telefono_representante,
						 "email_representante"=>$email_representante,
						 "categoria_producto"=>$categoria_producto,
						 "clientes_y_lugares"=>$clientes_y_lugares,
						 "precio_venta_final"=>$precio_venta_final,
						 "precio_compra"=>$precio_compra,
						 "descripcion_productos"=>$descripcion_productos,
						 "comentarios"=>$comentarios,
						 "fecha_registro"=>$fecha_registro);

	$aTypesToInsert = array("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s");

	$resultQuery = $wpdb->insert("usuarios_venta_productos",$aDataToSend,$aTypesToInsert);
	$idInsertRegistro = $wpdb->insert_id;


	/*Traemos el Estado*/
	$sqlForEstado = "SELECT t1.estado , t2.abr
					 FROM mo_codigos_postales AS t1
					 INNER JOIN estados_mexico AS t2 ON t2.estados_mexico_id = t1.estado_id
					 WHERE t1.codigo_postal = '".$codigo_postal."' LIMIT 1;";
	$aDataEstado = $wpdb->get_results($sqlForEstado);
	$sEstado =  $aDataEstado[0]->abr;
	$clave = $sEstado."-000".$idInsertRegistro."-ven";

	$htmlToSendData = '<html>
<head>
  <title>Modelorama</title>
  <style type="text/css">
    /**
     * START Responsive images
     */
    div, p, a, li, td {-webkit-text-size-adjust:none;-ms-text-size-adjust:none;}
    body {margin:0;padding:0;}
    table td {border-collapse:collapse;}
    .ExternalClass {width:100%;}
    .ExternalClass * {line-height: 110%}
    /**
     * END Responsive images
     */

    /**
     * START Mailchimp Styles
     */
    @media only screen and (max-width: 480px){
      .non-mobile{
        display: none!important;
      }
      .links a{
        display: block!important;
        text-align: center;
        margin: 1em 0px;
      }
      #templateColumns{
        width:100% !important;
      }
      .templateColumnContainer{
        display:block !important;
        width:100% !important;
      }
      .columnImage{
        height:auto !important;
        max-width:480px !important;
        width:100% !important;
      }
      .leftColumnContent{
        font-size:16px !important;
        line-height:125% !important;
      }
      .rightColumnContent{
        font-size:16px !important;
        line-height:125% !important;
      }
      .templateColumnContainer{
        display:block !important;
        width:100% !important;
      }
    }
    /**
     * END Mailchimp Styles
     */

  </style>

  <!--[if gte mso 15]>
  <style type="text/css" media="all">
    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
  </style>
  <![endif]-->
  <!--[if gte mso 9]>
  <style>
    #outlookholder {width:650px;}
  </style>
  <![endif]-->

</head>
<body margintop="0" marginleft="0" marginright="0" min-width:100%; width:100%;>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style=" display:block; max-width:650px !important;">
          <tr>
            <td>
              <table id="holder" class="wrapper" border="0" cellspacing="0" cellpadding="0" style="width:100%; max-width:650px !important;" align="center">
              <!--[if gte mso 9]>
              <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
              <![endif]-->
              <!--[if (IE)]>
              <table border="0" cellspacing="0" cellpadding="0" width="650" align="center"><tr><td>
              <![endif]-->
              <tr>
                <td>
                  <!--this is one way to set min width-->
                  <table width="320" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td width="320" align="center"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/spacer.gif" width="320" height="20" /></td>
                    </tr>
                  </table>

                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center">

                        <!-- START Mailchimp HTML -->
                        <table border="0" cellpadding="0" cellspacing="0" width="650" id="templateColumns">
                          <tr bgcolor="#f1c731">
                            <td align="center" valign="top">
                              <table align="left" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="leftColumnContent">
                                    <img src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/003.png" width="325" style="max-width:325px;" class="columnImage" />
                                  </td>
                                </tr>
                              </table>
                              <table align="right" border="0" cellpadding="0" cellspacing="0" width="325" class="non-mobile">
                                <tr>
                                  <td class="rightColumnContent">
                                    <img src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/004.png" width="325" style="max-width:325px;" class="columnImage" />
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="middle">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td align="right" style="color:#1b2741; font-family: Georgia, serif;">S&iacute;guenos en:</td>
                                  <td width="32"><a href="https://www.facebook.com/MiModelorama" target="_blank"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/icon-fb.png" width="32" height="32" /></a></td>
                                  <td width="10"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/spacer.gif" width="10" height="50" /></td>
                                  <td width="32"><a href="https://twitter.com/MiModelorama" target="_blank"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/icon-tw.png" width="32" height="32" /></a></td>
                                  <td width="32"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/spacer.gif" width="32" height="50" /></td>
                                </tr>
                              </table>
                              <hr style="border:0; height:0; border-top: 1px solid #f1f1f1; margin:0px 20px 20px; ">
                            </td>
                          </tr>
                          <tr>
                            <td style="color:#1b2741; font-family: Georgia, serif; padding: 0px 20px;">

                              <h1>Gracias por unirte a<br>comunidad Modelorama</h1>
                              <p>Muy Pronto nos pondremos en contacto contigo</p>
                              <p>Tu Clave de Registro: </p>
                              <p style="background-color:#f1c731; border:0px solid #1b2741; color:#1b2741; font-size:24px; font-weight: bold; padding:0.5em 0px; text-align: center;">'.$clave.'</p>

                              <hr style="border:0; height:0; border-top: 1px solid #f1f1f1; margin:0px 20px 20px; ">
                            </td>
                          </tr>
                          <tr bgcolor="#1b2741">
                            <td align="center" valign="top">
                              <table style="margin:10px 0px;" class="links">
                                <tr>
                                  <td>
                                    <font face="Arial, Helvetica, sans-serif" color="#fff">
                                      <a href="http://modelorama.com.mx/?page_id=2222" target="_blank" style="color:#fff; text-decoration: none; font-size:14px;">Aviso de Privacidad</a>
                                      <span style="margin:0px 15px;" class="non-mobile">|</span>
                                      <a href="mailto:contactomodelorama@gmodelo.com.mx" style="color:#fff; text-decoration: none; font-size:14px;">Contacto</a>
                                    </font>
                                  </td>
                                </tr>
                              </table>
                              <hr style="border:0; height:0; border-top: 1px solid #323d54; margin:0px 20px 20px; ">
                              <table align="left" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="leftColumnContent">
                                    <a href="https://www.amigosmodelo.com/" target="_blank"><img src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/logo-amigos-modelo.png" width="325" style="max-width:325px;" class="columnImage" /></a>
                                  </td>
                                </tr>
                              </table>
                              <table align="right" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="rightColumnContent">
                                    <a href="https://www.beerhouse.mx/" target="_blank"><img src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/logo-beer-house.png" width="325" style="max-width:325px;" class="columnImage" /></a>
                                  </td>
                                </tr>
                              </table>
                              <table width="100%" style="margin:20px 0px; padding-top:20px;">
                                <tr>
                                  <td align="center">
                                    <font face="Arial, Helvetica, sans-serif">
                                      <a href="#" style="color:#fff; text-decoration: none; font-size:14px;">Grupo Modelo 2015&copy;</a>
                                    </font>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!-- END Mailchimp HTML -->

                      </td>
                    </tr>
                  </table>

                  <table width="320" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td align="center"><img style="display:block;" border="0" src="http://modelorama.com.mx/wp-content/themes/pointfinder/mailing/spacer.gif" width="20" height="20" /></td>
                    </tr>
                  </table>

                </td>
              </tr>
              <!--[if mso]>
              </td></tr></table>
              <![endif]-->
              <!--[if gte mso 9]>
              </td></tr></table>
              <![endif]-->
              <table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>';

	// //Se Envia el Mail con la bienvenida
  $sEmailReceiver = $email_representante;
	$oSenmail1 = new SendMail();
	$oSendStatus   = $oSenmail1->enviar( $sEmailReceiver,  $htmlToSendData);


		/*Redireccionamos*/
		if($resultQuery==1){
			$_SESSION["saveVende"] = 1;
			header("Location:../../../../?page_id=3150");
			//echo "uno";
		}else{
			$_SESSION["saveVende"] = 0;
			echo "cero";
			//header("Location:../../../../?page_id=3150");
		}
	}//// END del Upload
}else{
	echo "Error el archivo de la curricula solo puede ser PDF, da click en el boton de atras y vuelve a subir el archivo de la curricula de la empresa en formato PDF";
}
}
/**
     * Clase para enviar el E-mail
     */
    class SendMail{
        private $_mail;

        public function __construct()
        {
					$this->_mail = new PHPMailer(true);
					$this->_mail->IsSMTP();
					$this->_mail->SMTPAuth   = true;
					//$this->_mail->SMTPSecure = "tls";
					$this->_mail->Host       = "mail.istheopencloud.com"; // SERVIDOR SMTP de correo
					$this->_mail->Port       =  25; //PUERTO DEL SERVIDOR SMTP de correo

					$this->_mail->Username   = "encuesta@istheopencloud.com"; // USERNAME del correo electronico que envia
					$this->_mail->Password   = "encuesta"; // PASSWORD del correo electronico que envia


					$this->_mail->From       = "reportes@modelorama.com.mx"; //Correo del que envia
					$this->_mail->FromName   = "Modelorama"; //Nombre del que envia

					$this->_mail->Subject    = "Tu inscripcion a Modelorama"; //Asunto del correo
					$this->_mail->CharSet    = 'UTF-8';
        }

        // Comentarios
        function enviar( $sEmailReceiver, $aData )
        {
            $sMessageMailing = $aData;

            try{
                $this->_mail->AddAddress( $sEmailReceiver ); // Destinatario

                $this->_mail->Body = $sMessageMailing; //Mensaje

                /*$this->_mail->AddAttachment( $aData['tmp_name'],
                                         $aData['name']);*/
				//$this->_mail->AddStringAttachment( $aData, date('YmdHis') . '.xls');

                $this->_mail->IsHTML(true); // Enviar en formato HTML

                $this->_mail->send();

                return true;
            }catch (phpmailerException $e) {
                echo $e->errorMessage(); //Pretty error messages from PHPMailer
                return false;
            }catch (Exception $e) {
                echo $e->getMessage(); //Boring error messages from anything else!
                return false;
            }
        }

    }


?>
