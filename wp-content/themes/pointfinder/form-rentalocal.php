
<?php
get_header();
	    #template name: Renta un Local
$idPost = get_the_ID();
?>
<script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#rentaLocal").validationEngine();

            var $ = jQuery;
        	var URLSite = '<?php bloginfo('template_url'); ?>';
          // funciones del controlador.
          var zip_codes = [];
          var zip_code;
          var states = [];
          var statex = "";
          var municipalities = [];
          var municipality;
          var colonies = [];
          var colony;

          // Obtener Codigo Postal.
          function getCodePostal() {

            $.ajax({
              method: "POST",
              url: URLSite+"/ajax/zip_codes.php",
              data: {codigo: $("#cp").val() },
              success: function(response){
                  var message = response["message"];
                  if(message == true){
                      zip_codes = response["result"];
                      state = zip_codes[0].estado;
                      municipality = zip_codes[0]["municipio"];
                      colonies = zip_codes;
                      setEstado(state);
                      setMunicipio(municipality);
                      setColonias(colonies);
                  }else{
                      alert("El codigo postal que envias no existe");
                      $("#cp").focus();
                      $("#edo").val("");
                      $("#mun").val("");
                      $("#col").empty();
                  }
              },
              error: function (request, status, error) {
                console.log(request.responseText);
              }
            });

          }

          function setEstado(state) {
            $("#edo").empty();
            $("#edo").val(state);
          }

          function setMunicipio(municip) {
            $("#mun").empty();
            $("#mun").val(municip);
          }

          function setColonias(colonias) {
            $("#col").empty();
            for(var index in colonias){
              $("#col").append('<option value="'+ colonias[index]["asentamiento"] +'">'+ colonias[index]["asentamiento"] +'</option>');
            }
          }

          // eventos de controles

          $("#cp").keyup(function(event) {
            if ( $(this).val().length == 5 ) {
              getCodePostal();
            }
          });

          $("#cp").focusout(function() {
              var cp_len = $(this).val().length;
              if(cp_len > 5 || cp_len < 5){
                  alert("Escribe un codigo postal de 5 digitos, para poder continuar con el registro.");
              }
          });

                 // jQuery para petición a repositorio de sepomex
                //     $.ajax({type:"GET",
                //       url:"https://sepomex-api.herokuapp.com/api/v1/zip_codes",
                //       data:{zip_code: cpValue},
                //       dataType:'json',
                //       success:function(response){
                //         var jsonObject = response['zip_codes'];
                //         console.log(jsonObject);
                //         $("#municipioResponse").val(jsonObject[0].d_mnpio);
                //         $("#estadoResponse").val(jsonObject[0].d_estado);
                //         var count = 0;
                //         $("#colonia").html('<option value="">Selecciona tu colonia</option>');
                //         if(jsonObject.length > 1) {
                //         $.each(jsonObject,function(key, value)
                //         {
                //           $("#colonia").append('<option value=' + jsonObject[count].d_asenta + '>' + jsonObject[count].d_asenta + '</option>');
                //           count++;
                //         });
                //       } else {
                //         $("#colonia").append('<option value=' + jsonObject[count].d_asenta + '>' + jsonObject[count].d_asenta + '</option>');
                //       }
                //       }
                //   });

        });

    </script>

<div class="wpf-container">
	<div id="pfmaincontent" class="wpf-container-inner"><div class="wpb_row vc_row-fluid page-renta">
		<div class="pf-fullwidth clearfix">
			<div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
				<div class="wpb_wrapper">

					<div class="wpb_text_column wpb_content_element ">
						<div class="wpb_wrapper">
							<h3>RENTA UN LOCAL</h3>

						</div>
					</div> <div class="wpb_row vc_row-fluid form-renta-holder">
					<div class="pf-container">
						<div class="pf-row">
							<div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
								<div class="wpb_wrapper">

									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<p class="question">¿Tienes un local comercial?</p>
											<p>Para que tu local sea candidato para ser Modelorama deberás cumplir con los siguientes requisitos:</p>
											<div class="form-holder">

												 <form class="form-opera" name="rentaLocal" id="rentaLocal" action="<?php bloginfo('template_url'); ?>/backends/saveRenta.php" method="POST">
                                                    <div class="campos-left">
                                                        <label for="nombre">Nombre</label>
                                                        <input type="text" name="nombre" id="nombre" class="validate[required,custom[onlyLetterSp]]" />
                                                        <label for="apellidoPaterno">Apellido paterno</label>
                                                        <input type="text" name="apellidoPaterno" id="apellidoPaterno" class="validate[required,custom[onlyLetterSp]]" />
                                                        <label for="apellidoMaterno">Apellido materno</label>
                                                        <input type="text" name="apellidoMaterno" id="apellidoMaterno" class="validate[required,custom[onlyLetterSp]]" />
                                                        <label for="correo">Correo electrónico</label>
                                                        <input type="text" name="correo" id="correo" class="validate[required,custom[email]]" />
                                                        <label for="telefonoFijo">Teléfono fijo</label>
                                                        <input type="text" name="telefonoFijo" id="telefonoFijo" class="validate[required]"  />
                                                        <label for="telefonoCelular">Teléfono celular</label>
                                                        <input type="text" name="telefonoCelular" id="telefonoCelular" class="validate[required]"  />
                                                        <label for="renta_solicitada">Renta Solicitada $</label>
                                                       <input type="text" name="renta_solicitada" id="renta_solicitada" class="validate[required, custom[number]]">
                                                        <label for="dimensiones">Dimensiones</label>
                                                        <input type="text" name="dimensiones" id="dimensiones" class="validate[required]">
                                                    </div>
                                                    <div class="campos-right">

                                                        <label for="calle_numero">Calle y Número</label>
                                                        <input type="text" name="calle_numero" id="calle_numero" class="validate[required]" />

                                                        <label for="cp">Código Postal</label>
                                                        <input type="text" name="cp" id="cp" class="validate[required,custom[integer]]" maxlength="5"/>

                                                        <label for="edo">Estado</label>
                                                        <input type="text" name="edo" id="edo" class="validate[required]" value="" />


                                                        <label for="mun">Municipio</label>
                                                        <input type="text" name="mun" id="mun" class="validate[required]" value=""/>


                                                        <label for="col">Colonia</label>
                                                        <div class="selec-input">
                                                            <select name="col" id="col" class="validate[required]">
                                                            	<option value="">Selecciona una Colonia</option>
                                                            </select>
                                                        </div>


                                                     	<label for="comentarios">Comentarios</label>
                                                     	<textarea name="comentarios" id="comentarios"></textarea>


                                                        <p class="extra">Existen otros requisitos que cumplir de los cuales serán informados personalmente.</p>
                                                        <p><input class="btn-enviar" type="submit" value="Registrate" /></p>

                                                    </div>
                                                            <input type="hidden" name="tipo" id="tipo" value="rentar" />
                                                            <input type="hidden" name="postID" id="postID" value="<?php echo $idPost; ?>">
                                                        </form>
											</div>

										</div>
									</div>
									<div class="wpb_text_column wpb_content_element ">
										<div class="wpb_wrapper">
											<div class="ul-local">
												<p>Características del local:</p>
												<ul class="local">
													<li>Uso de suelo comercial</li>
													<li>Al corriente en pagos (Agua, luz, predial)</li>
													<li>Bien ubicado, fácil acceso</li>
												</ul>
												<p>Documentación requerida:</p>
												<ul class="documentacion">
													<li>Copia de Identificación del Arrendador ( Credencial del INE vigente )</li>
													<li>Copia de Comprobante de Domicilio ( Recibo Telefónico Vigente )</li>
													<li>Copia de Estado de Cuenta Bancaria con CLABE Interbancaria para Transferencia</li>
													<li>Título de Propiedad</li>
													<li>Recibo de Arrendamiento vigente</li>
													<li>Copia de Uso de Suelo Comercial</li>
													<li>Copia de Pago Predial vigente</li>
													<li>Copia de Pago Agua vigente</li>
													<li>Copia de Pago Luz vigente</li>
													<li>Croquis simple de ubicación del Local</li>
													<li>Tamaño del Terreno en metros cuadrados</li>
													<li>Tamaño del Local en metros cuadrados</li>
													<li>Monto de la renta</li>
												</ul>
											</div>

										</div>
									</div>
								</div>
							</div>

						</div>
					</div></div>
				</div>
			</div>

		</div></div><!-- Row Backgrounds --><div class="upb_bg_img" data-ultimate-bg="url(http://projects.torodigital.com.mx/modelorama/wp-content/uploads/2015/04/bg-renta.jpg)" data-image-id="1886" data-ultimate-bg-style="vcpb-default" data-bg-img-repeat="repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="30" data-bg-override="0" data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="repeat" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"  data-custom-vc-row=""  data-vc="4.4.4"  data-theme-support=""   data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""    ></div>

	</div>
</div>
<div id="pf-membersystem-dialog"></div>
<a title="" class="pf-up-but"><i class="pfadmicon-glyph-859"></i></a>
<?php if(!isset($_GET["isRegister"])){ ?>
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_020;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_020;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<!-- Facebook Conversion Code for MODELORAMA_RENTATULOCAL_REGISTRO -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6037814790276', {'value':'0.00','currency':'MXN'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6037814790276&amp;cd[value]=0.00&amp;cd[currency]=MXN&amp;noscript=1" /></noscript>
<?php } ?>
<?php
get_footer();
?>
