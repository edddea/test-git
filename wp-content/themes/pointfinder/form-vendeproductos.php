

<?php
get_header();
	    #template name: Vende Productos
$idPost = get_the_ID();
?>
<script>
        jQuery(document).ready(function(){
        	var $ = jQuery;
        	var URLSite = '<?php bloginfo('template_url'); ?>';
            // binds form submission and fields to the validation engine
            jQuery("#vendeForm").validationEngine();

            // funciones del controlador.
            var zip_codes = [];
            var zip_code;
            var states = [];
            var statex = "";
            var municipalities = [];
            var municipality;
            var colonies = [];
            var colony;

            // Obtener Codigo Postal.
            // Obtener Codigo Postal.
            function getCodePostal() {

              $.ajax({
                method: "POST",
                url: URLSite+"/ajax/zip_codes.php",
                data: {codigo: $("#cpEmpresa").val() },
                success: function(response){
                    var message = response["message"];
                    if(message == true){
                        zip_codes = response["result"];
                        state = zip_codes[0].estado;
                        municipality = zip_codes[0]["municipio"];
                        colonies = zip_codes;
                        setEstado(state);
                        setMunicipio(municipality);
                        setColonias(colonies);
                    }else{
                        alert("El codigo postal que envias no existe");
                        $("#cp").focus();
                        $("#edo").val("");
                        $("#mun").val("");
                        $("#col").empty();
                    }
                },
                error: function (request, status, error) {
                  console.log(request.responseText);
                }
              });

            }

            function setEstado(state) {
              $("#edo").empty();
              $("#edo").val(state);
            }

            function setMunicipio(municip) {
              $("#mun").empty();
              $("#mun").val(municip);
            }

            function setColonias(colonias) {
              $("#col").empty();
              for(var index in colonias){
                $("#col").append('<option value="'+ colonias[index]["asentamiento"] +'">'+ colonias[index]["asentamiento"] +'</option>');
              }
            }

            // eventos de controles

            $("#cpEmpresa").keyup(function(event) {
              if ( $(this).val().length == 5 ) {
                getCodePostal();
              }
            });

            $("#cp").focusout(function() {
                var cp_len = $(this).val().length;
                if(cp_len > 5 || cp_len < 5){
                    alert("Escribe un codigo postal de 5 digitos, para poder continuar con el registro.");
                }
            });


        });

    </script>
        <div class="wpf-container">
        	<div id="pfmaincontent" class="wpf-container-inner"><div class="wpb_row vc_row-fluid title-vende">
		<div class="pf-fullwidth clearfix">
	<div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<h3>VENDE TUS PRODUCTOS</h3>

		</div>
	</div>
		</div>
	</div>

		</div></div><!-- Row Backgrounds --><div class="upb_bg_img" data-ultimate-bg="url(http://projects.torodigital.com.mx/modelorama/wp-content/uploads/2015/04/bg-title-vende.jpg)" data-image-id="1904" data-ultimate-bg-style="vcpb-default" data-bg-img-repeat="repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="30" data-bg-override="0" data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="repeat" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"  data-custom-vc-row=""  data-vc="4.4.4"  data-theme-support=""   data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""    ></div><div class="wpb_row vc_row-fluid content-vende">
		<div class="pf-container">
		<div class="pf-row">
	<div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">

	<div class="wpb_text_column wpb_content_element ">
		<div class="wpb_wrapper">
			<p class="vende-intro">Modelorama tiene el interés de realizar alianzas comerciales con proveedores que amplíen la gama de productos y servicios de calidad y de alto potencial de venta que satisfagan las necesidades de nuestros clientes.</p>
<p class="vende-intro">Si tienes interés de ofrecer algún producto o servicio completa el siguiente formulario.</p>

		</div>
	</div> <div class="wpb_row vc_row-fluid">
		<div class="pf-container">
		<div class="pf-row">
	<div class="col-lg-12 col-md-12 wpb_column vc_column_container ">
		<div class="wpb_wrapper">

	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
		<div class="wpb_wrapper">
			<div class="form-holder">


	<form enctype="multipart/form-data" class="form-vende" name="vendeForm" id="vendeForm"  action="<?php bloginfo('template_url'); ?>/backends/saveVentaProductos.php" method="POST" >
		<div class="datos datos-empresa">
			<h5>Datos de la empresa</h5>
			<div class="campos-holder campos-left">
				<p>
					<label for="nombreEmpresa">Nombre(s)</label>
					<input type="text" name="nombreEmpresa" id="nombreEmpresa" class="validate[required]">
				</p>
        <p><label for="rfc">RFC</label>
          <input type="text" name="rfc" id="rfc" class="validate[required]">
        </p>
        <p>
          <label for="telefonoEmpresa">Tel&eacute;fono</label>
          <input type="text" name="telefonoEmpresa" id="telefonoEmpresa" class="validate[required, custom[phone]]">
        </p>
				<p><label for="cpEmpresa">C&oacute;digo Postal</label>
					<input type="text" name="cpEmpresa" id="cpEmpresa" class="validate[required, custom[integer]]" maxlength="5" size="5">
				</p>
        <p><label for="edo">Estado</label>
          <input type="text" name="edo" id="edo" readonly class="validate[required]">
        </p>
			</div>
			<div class="campos-holder campos-right">

        <p><label for="mun">Municipio</label>
          <input type="text" name="mun" id="mun" readonly class="validate[required]">
        </p>
        <p>
        <label for="colonia">Colonia</label>
          <select name="col" id="col" class="validate[required]">
            <option value="">Selecciona tu Colonia</option>
          </select>
        </p>
        <p><label for="calle">Calle y n&uacute;mero</label>
          <input type="text" name="calle" id="calle" class="validate[required]">
        </p>
        <p><label for="cv">Curr&iacute;cula de la empresa (Solo PDF's)</label>
          <input type="file" name="cv" id="cv" class="validate[required]">
        </p>

			</div>
		</div>

		<div class="datos datos-representante">
			<h5>Datos del representante</h5>
			<div class="campos-holder campos-left">
				<p><label for="nombre">Nombre</label>
					<input type="text" name="nombre" id="nombre" class="validate[required,custom[onlyLetterSp]]">
				</p>
				<p><label for="correo">Correo electr&oacute;nico</label>
					<input type="text" name="correo" id="correo" class="validate[required, custom[email]]">
				</p>
			</div>
			<div class="campos-holder campos-right">
				<p><label for="telefono">Tel&eacute;fono</label>
					<input type="text" name="telefono" id="telefono" class="validate[required, custom[phone]]">
				</p>
			</div>
		</div>

		<div class="datos datos-producto">
			<h5>Producto</h5>
			<div class="campos-holder campos-left">
				<p><label for="categoriaProducto">Categor&iacute;a del producto</label>
					<input type="text" name="categoriaProducto" id="categoriaProducto" class="validate[required]">
				</p>
				<p><label for="clientesProducto">Clientes y lugares donde se vende el producto</label>
					<input type="text" name="clientesProducto" id="clientesProducto" class="validate[required]">
				</p>
				<p><label for="precioFinal">Precio de venta al consumidor final</label>
					<input type="text" name="precioFinal" id="precioFinal" class="validate[required]">
				</p>
				<p class="comenta1">
					<label for="comentarios">Comentarios</label>
					<textarea class="comentarios" name="comentarios" id="comentarios" class="validate[required]"></textarea>
				</p>
			</div>
			<div class="campos-holder campos-right">
				<p><label for="descripcionProducto">Descripci&oacute;n del producto o productos</label>
					<!--<input type="text" name="descripcionProducto" id="descripcionProducto" class="validate[required]">-->
					<textarea name="descripcionProducto" id="descripcionProducto" class="validate[required]"></textarea>
				</p>
				<p><label for="precioCompra">Precio de compra</label>
					<input type="text" name="precioCompra" id="precioCompra" class="validate[required]">
				</p>
				<p class="comenta2"><label for="comentarios2">Comentarios</label>
					<textarea class="comentarios" name="comentarios2" id="comentarios2"></textarea>
				</p>
				<p class="parrafo-enviar"><input class="btn-enviar" type="submit" value="Reg&iacute;strate"></p>
			</div>
		</div>
		<input type="hidden" name="postID" id="postID" value="<?php echo $idPost; ?>">
	</form>
</div>
		</div>
	</div>
		</div>
	</div>

		</div>
		</div></div>
		</div>
	</div>

		</div>
		</div></div><!-- Row Backgrounds --><div class="upb_no_bg" data-fadeout="" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"  data-custom-vc-row=""  data-vc="4.4.4"  data-theme-support=""    ></div>

        </div>
        </div>
        <div id="pf-membersystem-dialog"></div>
        <a title="" class="pf-up-but"><i class="pfadmicon-glyph-859"></i></a>
        <?php if(!isset($_GET["isRegister"])){ ?>
        <script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_030;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://4531948.fls.doubleclick.net/activityi;src=4531948;type=model002;cat=mx_030;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

<!-- Facebook Conversion Code for MODELORAMA_VENDE TUS PRODUCTOS_REGISTRO -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6037815153076', {'value':'0.00','currency':'MXN'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6037815153076&amp;cd[value]=0.00&amp;cd[currency]=MXN&amp;noscript=1" /></noscript>
<?php } ?>
        <?php
get_footer();
?>
