jQuery(document).ready(function() {

    var $ = jQuery;
    // binds form submission and fields to the validation engine
    $("#operaForm").validationEngine();

    // funciones del controlador.
    var zip_codes = [];
    var state = "";
    var municipality;
    var colonies = [];
    var preguntas = [];
    var respuestas = [];

    // Obtener Codigo Postal.
    function getCodePostal() {

        $.ajax({
            method: "POST",
            url: templateUrl + "/ajax/zip_codes.php",
            data: {
                codigo: $("#cp").val()
            },
            success: function(response) {
                response = $.parseJSON(response);
                var message = response["message"];

                if(message == true){
                    zip_codes = response["result"];
                    state = zip_codes[0].estado;
                    municipality = zip_codes[0]["municipio"];
                    colonies = zip_codes;
                    setEstado(state);
                    setMunicipio(municipality);
                    setColonias(colonies);
                }else{
                    alert("El codigo postal que envias no existe");
                    $("#cp").focus();
                    $("#edo").val("");
                    $("#mun").val("");
                    $("#col").empty();
                }

            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });

    }

    // Obtener Preguntas.
    function getPreguntas() {

        $.ajax({
            method: "POST",
            url: templateUrl + "/ajax/getPreguntas.php",
            data: {formulario: 'winning_profile'},
            success: function(response) {
                response = $.parseJSON(response);
                var message = response["message"];

                if(message == true){
                  preguntas = response["result"];

                  for(index in preguntas){
                      getRespuestas(preguntas[index]["id"]);
                      setFormulario(preguntas[index], respuestas, index);
                  }

                  $("input[type='radio']").click(function(){
                    var rtext = this.nextSibling.data;
                    var nid = $(this).attr("id").split("r");

                    if(rtext.trim() == "Otra (especifica)"){
                      $(this).after("<input type='text' name=''>");
                    }

                    if($(this).val() == "Otra (especifica)"){
                      $(this).append("&nbsp;&nbsp;<input type='text' name='' id='c"+ nid[1]+"' value='yaaa'>&nbsp;&nbsp;");
                    }
                  });

                  // $("#l7, #r7").hide();
                  // //$("#r7").nextSibling.hide();
                  // $("#l8, #r8").hide();
                  // //$("#r8").nextSibling.hide();
                  // $("#l9, #r9").hide();
                  // //$("#r9").nextSibling.hide();
                  //
                  // $("#r6").click(function(){
                  //   var rtext = this.nextSibling.data;
                  //
                  //   if(rtext.trim() == "Si"){
                  //     console.log("si");
                  //     $("#l7, #r7").show();
                  //     //$("#r7").nextSibling.show();
                  //     $("#l8, #r8").show();
                  //     //$("#r8").nextSibling.show();
                  //     $("#l9, #r9").hide();
                  //     //$("#r9").nextSibling.hide();
                  //   }else{
                  //     console.log("no");
                  //     $("#l7, #r7").hide();
                  //     //$("#r7").nextSibling.hide();
                  //     $("#l8, #r8").hide();
                  //     //$("#r8").nextSibling.hide();
                  //     $("#l9, #r9").show();
                  //     //$("#r9").nextSibling.show();
                  //   }
                  //});

                }else{
                    alert("Error al conseguir preguntas");
                }

            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });

    }


    // Obtener Respuestas por Pregunta.
    function getRespuestas(pregunta_id) {

        $.ajax({
            method: "POST",
            url: templateUrl + "/ajax/getRespuestas.php",
            data: {pregunta_id: pregunta_id},
            async: false,
            success: function(response) {
                response = $.parseJSON(response);
                var message = response["message"];

                if(message == true){
                  respuestas = response["result"];
                }else{
                    alert("Error al conseguir respuestas");
                }

            },
            error: function(request, status, error) {
                console.log(request.responseText);
            }
        });

    }

    function setEstado(state) {
        $("#edo").empty();
        $("#edo").val(state);
    }

    function setMunicipio(municip) {
        $("#mun").empty();
        $("#mun").val(municip);
    }

    function setColonias(colonias) {
        $("#col").empty();
        for (var index in colonias) {
            $("#col").append("<option value='" + colonias[index]["asentamiento"] + "'>" + colonias[index]["asentamiento"] + "</option>");
        }
    }

    function setFormulario(pregunta, respuestas, con) {
      if(con % 2 == 0){
        var html_left = "";
        $("#colum-left").append("<br><label id='l"+ pregunta["id"] +"'>"+ pregunta["pregunta"] +"</label><br>");
        $("#colum-left").append("<input type='hidden' name='p"+ pregunta["id"] +"' value='"+ pregunta["id"]+ "'>");

        for (var index in respuestas) {
          html_left += "<input type='radio' id='r"+ pregunta["id"] +"' name='r"+ pregunta["id"] +"' value='"+ respuestas[index]["id"] +"'>&nbsp;&nbsp;"+ respuestas[index]["respuesta"]+ "<br>";
        }

        $("#colum-left").append(html_left);
      }else{
        var html_right = "";
        $("#colum-right").append("<br><label id='l"+ pregunta["id"] +"'>"+ pregunta["pregunta"] +"</label><br>");
        $("#colum-right").append("<input type='hidden' name='p"+ pregunta["id"] +"' value='"+ pregunta["id"]+ "'>");

        for (var index in respuestas) {
          html_right += "<input type='radio' id='r"+ pregunta["id"] +"' name='r"+ pregunta["id"] +"' value='"+ respuestas[index]["id"] +"'>&nbsp;&nbsp;"+ respuestas[index]["respuesta"]+ "<br>";
        }

        $("#colum-right").append(html_right);
      }
    }

    function init() {
      getPreguntas();
    }

    init();

    // eventos de controles

    $("input[type='radio']").click(function(){
      console.log("click radio");
      if($(this).val() == "Otra (especifica)"){
        $(this).append("<input type='text' name='' >");
      }
    });

    $("#r6").click(function(){
      var rtext = this.nextSibling.data;

      if(rtext.trim() == "Si"){
        $("#l7, #r7").show();
        //$("#r7").nextSibling.show();
        $("#l8, #r8").show();
        //$("#r8").nextSibling.show();
        $("#l9, #r9").hide();
        //$("#r9").nextSibling.hide();
      }else if(rtext.trim() == "No"){
        $("#l7, #r7").hide();
        //$("#r7").nextSibling.hide();
        $("#l8, #r8").hide();
        //$("#r8").nextSibling.hide();
        $("#l9, #r9").show();
        //$("#r9").nextSibling.show();
      }
    });

    $("#cp").keyup(function(event) {
        var cp_len = $(this).val().length;
        if ( cp_len == 5) {
            getCodePostal();
        }
    });

    $("#cp").focusout(function() {
        var cp_len = $(this).val().length;
        if(cp_len > 5 || cp_len < 5){
            alert("Escribe un codigo postal de 5 digitos, para poder continuar con el registro.");
        }
    });

});
