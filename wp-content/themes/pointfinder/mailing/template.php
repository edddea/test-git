<html>
<head>
  <title>Modelorama</title>
  <style type="text/css">
    /**
     * START Responsive images
     */
    div, p, a, li, td {-webkit-text-size-adjust:none;-ms-text-size-adjust:none;}
    body {margin:0;padding:0;}
    table td {border-collapse:collapse;}
    .ExternalClass {width:100%;}
    .ExternalClass * {line-height: 110%}
    /**
     * END Responsive images
     */
    
    /**
     * START Mailchimp Styles
     */
    @media only screen and (max-width: 480px){
      .non-mobile{
        display: none!important;
      }
      .links a{
        display: block!important;
        text-align: center;
        margin: 1em 0px;
      }
      #templateColumns{
        width:100% !important;
      }
      .templateColumnContainer{
        display:block !important;
        width:100% !important;
      }
      .columnImage{
        height:auto !important;
        max-width:480px !important;
        width:100% !important;
      }
      .leftColumnContent{
        font-size:16px !important;
        line-height:125% !important;
      }
      .rightColumnContent{
        font-size:16px !important;
        line-height:125% !important;
      }
      .templateColumnContainer{
        display:block !important;
        width:100% !important;
      }
    }
    /**
     * END Mailchimp Styles
     */

  </style>

  <!--[if gte mso 15]>
  <style type="text/css" media="all">
    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
  </style>
  <![endif]-->
  <!--[if gte mso 9]>
  <style>
    #outlookholder {width:650px;}
  </style>
  <![endif]-->

</head>
<body margintop="0" marginleft="0" marginright="0" min-width:100%; width:100%;>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style=" display:block; max-width:650px !important;">
          <tr>
            <td>
              <table id="holder" class="wrapper" border="0" cellspacing="0" cellpadding="0" style="width:100%; max-width:650px !important;" align="center">
              <!--[if gte mso 9]>
              <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
              <![endif]-->
              <!--[if (IE)]>
              <table border="0" cellspacing="0" cellpadding="0" width="650" align="center"><tr><td>
              <![endif]-->
              <tr>
                <td> 
                  <!--this is one way to set min width-->
                  <table width="320" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td width="320" align="center"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/spacer.gif" width="320" height="20" /></td>
                    </tr>
                  </table>

                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="center">
                      
                        <!-- START Mailchimp HTML -->
                        <table border="0" cellpadding="0" cellspacing="0" width="650" id="templateColumns">
                          <tr bgcolor="#f1c731">
                            <td align="center" valign="top">
                              <table align="left" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="leftColumnContent">
                                    <img src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/003.png" width="325" style="max-width:325px;" class="columnImage" />
                                  </td>
                                </tr>
                              </table>
                              <table align="right" border="0" cellpadding="0" cellspacing="0" width="325" class="non-mobile">
                                <tr>
                                  <td class="rightColumnContent">
                                    <img src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/004.png" width="325" style="max-width:325px;" class="columnImage" />
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="middle">
                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                  <td align="right" style="color:#1b2741; font-family: Georgia, serif;">S&iacute;guenos en:</td>
                                  <td width="32"><a href="https://www.facebook.com/MiModelorama" target="_blank"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/icon-fb.png" width="32" height="32" /></a></td>
                                  <td width="10"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/spacer.gif" width="10" height="50" /></td>
                                  <td width="32"><a href="https://twitter.com/MiModelorama" target="_blank"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/icon-tw.png" width="32" height="32" /></a></td>
                                  <td width="32"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/spacer.gif" width="32" height="50" /></td>
                                </tr>
                              </table>
                              <hr style="border:0; height:0; border-top: 1px solid #f1f1f1; margin:0px 20px 20px; ">
                            </td>
                          </tr>
                          <tr>
                            <td style="color:#1b2741; font-family: Georgia, serif; padding: 0px 20px;">
                              
                              <h1>Gracias por unirte a<br>comunidad Modelorama</h1>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia enim non dolorem voluptatem rem temporibus accusantium, distinctio facilis, blanditiis. Esse vitae numquam officiis eveniet impedit sit voluptatum praesentium itaque. Magni, esse. Beatae animi neque explicabo earum ea voluptatem debitis blanditiis esse repudiandae est quia dignissimos, dolores voluptas aut repellat! Dicta!</p>
                              <p style="background-color:#f1c731; border:0px solid #1b2741; color:#1b2741; font-size:24px; font-weight: bold; padding:0.5em 0px; text-align: center;">JAL-00000001</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia enim non dolorem voluptatem rem temporibus accusantium, distinctio facilis, blanditiis. Esse vitae numquam officiis eveniet impedit sit voluptatum praesentium itaque. Magni, esse. Beatae animi neque explicabo earum ea voluptatem debitis blanditiis esse repudiandae est quia dignissimos, dolores voluptas aut repellat! Dicta!</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia enim non dolorem voluptatem rem temporibus accusantium, distinctio facilis, blanditiis. Esse vitae numquam officiis eveniet impedit sit voluptatum praesentium itaque. Magni, esse. Beatae animi neque explicabo earum ea voluptatem debitis blanditiis esse repudiandae est quia dignissimos, dolores voluptas aut repellat! Dicta!</p>
                              
                              <hr style="border:0; height:0; border-top: 1px solid #f1f1f1; margin:0px 20px 20px; ">
                            </td>
                          </tr>
                          <tr bgcolor="#1b2741">
                            <td align="center" valign="top">
                              <table style="margin:10px 0px;" class="links">
                                <tr>
                                  <td>
                                    <font face="Arial, Helvetica, sans-serif" color="#fff">
                                      <a href="http://modelorama.com.mx/?page_id=2222" target="_blank" style="color:#fff; text-decoration: none; font-size:14px;">Aviso de Privacidad</a>
                                      <span style="margin:0px 15px;" class="non-mobile">|</span>
                                      <a href="mailto:contactomodelorama@gmodelo.com.mx" style="color:#fff; text-decoration: none; font-size:14px;">Contacto</a>
                                    </font>
                                  </td> 
                                </tr>
                              </table>
                              <hr style="border:0; height:0; border-top: 1px solid #323d54; margin:0px 20px 20px; ">
                              <table align="left" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="leftColumnContent">
                                    <a href="https://www.amigosmodelo.com/" target="_blank"><img src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/logo-amigos-modelo.png" width="325" style="max-width:325px;" class="columnImage" /></a>
                                  </td>
                                </tr>
                              </table>
                              <table align="right" border="0" cellpadding="0" cellspacing="0" width="325" class="templateColumnContainer">
                                <tr>
                                  <td class="rightColumnContent">
                                    <a href="https://www.beerhouse.mx/" target="_blank"><img src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/logo-beer-house.png" width="325" style="max-width:325px;" class="columnImage" /></a>
                                  </td>
                                </tr>
                              </table>
                              <table width="100%" style="margin:20px 0px; padding-top:20px;">
                                <tr>
                                  <td align="center">
                                    <font face="Arial, Helvetica, sans-serif">
                                      <a href="#" style="color:#fff; text-decoration: none; font-size:14px;">Grupo Modelo 2015&copy;</a>
                                    </font>
                                  </td> 
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!-- END Mailchimp HTML -->

                      </td>
                    </tr>
                  </table>

                  <table width="320" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td align="center"><img style="display:block;" border="0" src="http://modelorama.com.mx/development/wp-content/themes/pointfinder/mailing/spacer.gif" width="20" height="20" /></td>
                    </tr>
                  </table>

                </td>
              </tr>
              <!--[if mso]>
              </td></tr></table>
              <![endif]-->
              <!--[if gte mso 9]>
              </td></tr></table>
              <![endif]-->
              <table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>