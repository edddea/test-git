<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// Composer
require __DIR__ . '/vendor/autoload.php';

// Dotenv
$dotenv = new Dotenv\Dotenv( ABSPATH );

try {
	$dotenv->load();
} catch (Exception $e) {
	$e->getMessage();
}

header('X-Frame-Options: SAMEORIGIN');
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
#define('DB_NAME', getenv('DB_NAME'));
define('DB_NAME', 'modelorama_testing');

/** MySQL database username */
#define('DB_USER', getenv('DB_USER'));
define('DB_USER', 'root');

/** MySQL database password */
#define('DB_PASSWORD', getenv('DB_PASSWORD'));
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
#define('DB_HOST', getenv('DB_HOST'));
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',			getenv('AUTH_KEY'));
define('SECURE_AUTH_KEY',	getenv('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY',		getenv('LOGGED_IN_KEY'));
define('NONCE_KEY',			getenv('NONCE_KEY'));
define('AUTH_SALT',			getenv('AUTH_SALT'));
define('SECURE_AUTH_SALT',	getenv('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT',	getenv('LOGGED_IN_SALT'));
define('NONCE_SALT',		getenv('NONCE_SALT'));

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */

if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
	$_SERVER['HTTPS'] = 'on';

$protocol = 'http://';

if ( isset($_SERVER['HTTPS']) ) {

	if ( 'on' == strtolower($_SERVER['HTTPS']) )
		$protocol = 'https://';

	if ( '1' == $_SERVER['HTTPS'] ) {
		$protocol = 'https://';
	} elseif ( isset($_SERVER['SERVER_PORT']) && ( '443' == $_SERVER['SERVER_PORT'] ) ) {
		$protocol = 'https://';
	}

}

define('FORCE_SSL_ADMIN', true);
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
	$_SERVER['HTTPS']='on';

define('WP_DEBUG', getenv('WP_DEBUG'));

define( 'WP_HOME', 'https://' . getenv( 'HOST_NAME') );
define( 'WP_SITEURL', 'https://' . getenv( 'HOST_NAME' ) );
define( 'WP_CONTENT_URL', '/wp-content' );
define( 'DOMAIN_CURRENT_SITE', $_SERVER[ 'HOST_NAME' ] );
define('RELOCATE', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
